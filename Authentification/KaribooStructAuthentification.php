<?php
/**
 * File for class KaribooStructAuthentification
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructAuthentification originally named Authentification
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd3}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructAuthentification extends KaribooWsdlClass
{
    /**
     * The UserName
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $UserName;
    /**
     * The Password
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Password;
    /**
     * Constructor method for Authentification
     * @see parent::__construct()
     * @param string $_userName
     * @param string $_password
     * @return KaribooStructAuthentification
     */
    public function __construct($_userName = NULL,$_password = NULL)
    {
        parent::__construct(array('UserName'=>$_userName,'Password'=>$_password),false);
    }
    /**
     * Get UserName value
     * @return string|null
     */
    public function getUserName()
    {
        return $this->UserName;
    }
    /**
     * Set UserName value
     * @param string $_userName the UserName
     * @return string
     */
    public function setUserName($_userName)
    {
        return ($this->UserName = $_userName);
    }
    /**
     * Get Password value
     * @return string|null
     */
    public function getPassword()
    {
        return $this->Password;
    }
    /**
     * Set Password value
     * @param string $_password the Password
     * @return string
     */
    public function setPassword($_password)
    {
        return ($this->Password = $_password);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructAuthentification
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
