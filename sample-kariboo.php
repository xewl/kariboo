<?php
/**
 * Test with Kariboo for 'http://api.kariboo.be/service.svc?wsdl'
 * @package Kariboo
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
ini_set('memory_limit','512M');
ini_set('display_errors',true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/KaribooAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 * 
 * This is an associative array as:
 * - the key must be a KaribooWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 * 
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[KaribooWsdlClass::WSDL_URL] = 'http://api.kariboo.be/service.svc?wsdl';
 * $wsdl[KaribooWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[KaribooWsdlClass::WSDL_TRACE] = true;
 * $wsdl[KaribooWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[KaribooWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as: 
 * - $wsdlObject = new KaribooWsdlClass($wsdl);
 */
/**
 * Examples
 */


/********************************
 * Example for KaribooServiceSpot
 */
$karibooServiceSpot = new KaribooServiceSpot();
// sample call for KaribooServiceSpot::setSoapHeaderAuthentification() in order to initialize required SoapHeader
$karibooServiceSpot->setSoapHeaderAuthentification(new KaribooStructAuthentification(/*** update parameters list ***/));
// sample call for KaribooServiceSpot::SpotSelector()
if($karibooServiceSpot->SpotSelector(new KaribooStructSpotAroundMeRequest(/*** update parameters list ***/)))
    print_r($karibooServiceSpot->getResult());
else
    print_r($karibooServiceSpot->getLastError());
// sample call for KaribooServiceSpot::SpotInformation()
if($karibooServiceSpot->SpotInformation(new KaribooStructSpotRequest(/*** update parameters list ***/)))
    print_r($karibooServiceSpot->getResult());
else
    print_r($karibooServiceSpot->getLastError());

/**********************************
 * Example for KaribooServiceClient
 */
$karibooServiceClient = new KaribooServiceClient();
// sample call for KaribooServiceClient::setSoapHeaderAuthentification() in order to initialize required SoapHeader
$karibooServiceClient->setSoapHeaderAuthentification(new KaribooStructAuthentification(/*** update parameters list ***/));
// sample call for KaribooServiceClient::ClientBarCodeWebTrackingData()
if($karibooServiceClient->ClientBarCodeWebTrackingData(new KaribooStructTrackingRequest(/*** update parameters list ***/)))
    print_r($karibooServiceClient->getResult());
else
    print_r($karibooServiceClient->getLastError());

/************************************
 * Example for KaribooServiceAnnounce
 */
$karibooServiceAnnounce = new KaribooServiceAnnounce();
// sample call for KaribooServiceAnnounce::setSoapHeaderAuthentification() in order to initialize required SoapHeader
$karibooServiceAnnounce->setSoapHeaderAuthentification(new KaribooStructAuthentification(/*** update parameters list ***/));
// sample call for KaribooServiceAnnounce::AnnounceParcel()
if($karibooServiceAnnounce->AnnounceParcel(new KaribooStructAnnounceRequest(/*** update parameters list ***/)))
    print_r($karibooServiceAnnounce->getResult());
else
    print_r($karibooServiceAnnounce->getLastError());

/*************************************
 * Example for KaribooServiceFinancial
 */
$karibooServiceFinancial = new KaribooServiceFinancial();
// sample call for KaribooServiceFinancial::setSoapHeaderAuthentification() in order to initialize required SoapHeader
$karibooServiceFinancial->setSoapHeaderAuthentification(new KaribooStructAuthentification(/*** update parameters list ***/));
// sample call for KaribooServiceFinancial::FinancialParcel()
if($karibooServiceFinancial->FinancialParcel(new KaribooStructFinancialParcelRequest(/*** update parameters list ***/)))
    print_r($karibooServiceFinancial->getResult());
else
    print_r($karibooServiceFinancial->getLastError());
