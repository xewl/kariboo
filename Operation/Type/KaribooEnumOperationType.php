<?php
/**
 * File for class KaribooEnumOperationType
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooEnumOperationType originally named OperationType
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooEnumOperationType extends KaribooWsdlClass
{
    /**
     * Constant for value 'C'
     * @return string 'C'
     */
    const VALUE_C = 'C';
    /**
     * Constant for value 'D'
     * @return string 'D'
     */
    const VALUE_D = 'D';
    /**
     * Constant for value 'U'
     * @return string 'U'
     */
    const VALUE_U = 'U';
    /**
     * Return true if value is allowed
     * @uses KaribooEnumOperationType::VALUE_C
     * @uses KaribooEnumOperationType::VALUE_D
     * @uses KaribooEnumOperationType::VALUE_U
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(KaribooEnumOperationType::VALUE_C,KaribooEnumOperationType::VALUE_D,KaribooEnumOperationType::VALUE_U));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
