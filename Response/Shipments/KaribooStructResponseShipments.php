<?php
/**
 * File for class KaribooStructResponseShipments
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructResponseShipments originally named ResponseShipments
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructResponseShipments extends KaribooWsdlClass
{
    /**
     * The Barcode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Barcode;
    /**
     * The BarcodeReturn
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $BarcodeReturn;
    /**
     * The label
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var base64Binary
     */
    public $label;
    /**
     * The labelReturn
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var base64Binary
     */
    public $labelReturn;
    /**
     * Constructor method for ResponseShipments
     * @see parent::__construct()
     * @param string $_barcode
     * @param string $_barcodeReturn
     * @param base64Binary $_label
     * @param base64Binary $_labelReturn
     * @return KaribooStructResponseShipments
     */
    public function __construct($_barcode = NULL,$_barcodeReturn = NULL,$_label = NULL,$_labelReturn = NULL)
    {
        parent::__construct(array('Barcode'=>$_barcode,'BarcodeReturn'=>$_barcodeReturn,'label'=>$_label,'labelReturn'=>$_labelReturn),false);
    }
    /**
     * Get Barcode value
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->Barcode;
    }
    /**
     * Set Barcode value
     * @param string $_barcode the Barcode
     * @return string
     */
    public function setBarcode($_barcode)
    {
        return ($this->Barcode = $_barcode);
    }
    /**
     * Get BarcodeReturn value
     * @return string|null
     */
    public function getBarcodeReturn()
    {
        return $this->BarcodeReturn;
    }
    /**
     * Set BarcodeReturn value
     * @param string $_barcodeReturn the BarcodeReturn
     * @return string
     */
    public function setBarcodeReturn($_barcodeReturn)
    {
        return ($this->BarcodeReturn = $_barcodeReturn);
    }
    /**
     * Get label value
     * @return base64Binary|null
     */
    public function getLabel()
    {
        return $this->label;
    }
    /**
     * Set label value
     * @param base64Binary $_label the label
     * @return base64Binary
     */
    public function setLabel($_label)
    {
        return ($this->label = $_label);
    }
    /**
     * Get labelReturn value
     * @return base64Binary|null
     */
    public function getLabelReturn()
    {
        return $this->labelReturn;
    }
    /**
     * Set labelReturn value
     * @param base64Binary $_labelReturn the labelReturn
     * @return base64Binary
     */
    public function setLabelReturn($_labelReturn)
    {
        return ($this->labelReturn = $_labelReturn);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructResponseShipments
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
