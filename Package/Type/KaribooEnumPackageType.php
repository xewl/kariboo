<?php
/**
 * File for class KaribooEnumPackageType
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooEnumPackageType originally named PackageType
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooEnumPackageType extends KaribooWsdlClass
{
    /**
     * Constant for value 'Normal'
     * @return string 'Normal'
     */
    const VALUE_NORMAL = 'Normal';
    /**
     * Constant for value 'Heavy'
     * @return string 'Heavy'
     */
    const VALUE_HEAVY = 'Heavy';
    /**
     * Constant for value 'Large'
     * @return string 'Large'
     */
    const VALUE_LARGE = 'Large';
    /**
     * Constant for value 'OverSize'
     * @return string 'OverSize'
     */
    const VALUE_OVERSIZE = 'OverSize';
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Return true if value is allowed
     * @uses KaribooEnumPackageType::VALUE_NORMAL
     * @uses KaribooEnumPackageType::VALUE_HEAVY
     * @uses KaribooEnumPackageType::VALUE_LARGE
     * @uses KaribooEnumPackageType::VALUE_OVERSIZE
     * @uses KaribooEnumPackageType::VALUE_NONE
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(KaribooEnumPackageType::VALUE_NORMAL,KaribooEnumPackageType::VALUE_HEAVY,KaribooEnumPackageType::VALUE_LARGE,KaribooEnumPackageType::VALUE_OVERSIZE,KaribooEnumPackageType::VALUE_NONE));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
