<?php
/**
 * File for class KaribooStructSpotResponse
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructSpotResponse originally named SpotResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructSpotResponse extends KaribooWsdlClass
{
    /**
     * The PickUpPoints
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfPickUpPoint
     */
    public $PickUpPoints;
    /**
     * Constructor method for SpotResponse
     * @see parent::__construct()
     * @param KaribooStructArrayOfPickUpPoint $_pickUpPoints
     * @return KaribooStructSpotResponse
     */
    public function __construct($_pickUpPoints = NULL)
    {
        parent::__construct(array('PickUpPoints'=>($_pickUpPoints instanceof KaribooStructArrayOfPickUpPoint)?$_pickUpPoints:new KaribooStructArrayOfPickUpPoint($_pickUpPoints)),false);
    }
    /**
     * Get PickUpPoints value
     * @return KaribooStructArrayOfPickUpPoint|null
     */
    public function getPickUpPoints()
    {
        return $this->PickUpPoints;
    }
    /**
     * Set PickUpPoints value
     * @param KaribooStructArrayOfPickUpPoint $_pickUpPoints the PickUpPoints
     * @return KaribooStructArrayOfPickUpPoint
     */
    public function setPickUpPoints($_pickUpPoints)
    {
        return ($this->PickUpPoints = $_pickUpPoints);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructSpotResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
