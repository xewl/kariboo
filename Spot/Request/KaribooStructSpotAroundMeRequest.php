<?php
/**
 * File for class KaribooStructSpotAroundMeRequest
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructSpotAroundMeRequest originally named SpotAroundMeRequest
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructSpotAroundMeRequest extends KaribooWsdlClass
{
    /**
     * The Latitude
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var float
     */
    public $Latitude;
    /**
     * The Longitude
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var float
     */
    public $Longitude;
    /**
     * The PostalCode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $PostalCode;
    /**
     * The NumberSpotsToReturn
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $NumberSpotsToReturn;
    /**
     * The OpenOnSunday
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $OpenOnSunday;
    /**
     * The OpenAfter1600
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $OpenAfter1600;
    /**
     * Constructor method for SpotAroundMeRequest
     * @see parent::__construct()
     * @param float $_latitude
     * @param float $_longitude
     * @param string $_postalCode
     * @param string $_numberSpotsToReturn
     * @param KaribooEnumYesNo $_openOnSunday
     * @param KaribooEnumYesNo $_openAfter1600
     * @return KaribooStructSpotAroundMeRequest
     */
    public function __construct($_latitude = NULL,$_longitude = NULL,$_postalCode = NULL,$_numberSpotsToReturn = NULL,$_openOnSunday = NULL,$_openAfter1600 = NULL)
    {
        parent::__construct(array('Latitude'=>$_latitude,'Longitude'=>$_longitude,'PostalCode'=>$_postalCode,'NumberSpotsToReturn'=>$_numberSpotsToReturn,'OpenOnSunday'=>$_openOnSunday,'OpenAfter1600'=>$_openAfter1600),false);
    }
    /**
     * Get Latitude value
     * @return float|null
     */
    public function getLatitude()
    {
        return $this->Latitude;
    }
    /**
     * Set Latitude value
     * @param float $_latitude the Latitude
     * @return float
     */
    public function setLatitude($_latitude)
    {
        return ($this->Latitude = $_latitude);
    }
    /**
     * Get Longitude value
     * @return float|null
     */
    public function getLongitude()
    {
        return $this->Longitude;
    }
    /**
     * Set Longitude value
     * @param float $_longitude the Longitude
     * @return float
     */
    public function setLongitude($_longitude)
    {
        return ($this->Longitude = $_longitude);
    }
    /**
     * Get PostalCode value
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->PostalCode;
    }
    /**
     * Set PostalCode value
     * @param string $_postalCode the PostalCode
     * @return string
     */
    public function setPostalCode($_postalCode)
    {
        return ($this->PostalCode = $_postalCode);
    }
    /**
     * Get NumberSpotsToReturn value
     * @return string|null
     */
    public function getNumberSpotsToReturn()
    {
        return $this->NumberSpotsToReturn;
    }
    /**
     * Set NumberSpotsToReturn value
     * @param string $_numberSpotsToReturn the NumberSpotsToReturn
     * @return string
     */
    public function setNumberSpotsToReturn($_numberSpotsToReturn)
    {
        return ($this->NumberSpotsToReturn = $_numberSpotsToReturn);
    }
    /**
     * Get OpenOnSunday value
     * @return KaribooEnumYesNo|null
     */
    public function getOpenOnSunday()
    {
        return $this->OpenOnSunday;
    }
    /**
     * Set OpenOnSunday value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_openOnSunday the OpenOnSunday
     * @return KaribooEnumYesNo
     */
    public function setOpenOnSunday($_openOnSunday)
    {
        if(!KaribooEnumYesNo::valueIsValid($_openOnSunday))
        {
            return false;
        }
        return ($this->OpenOnSunday = $_openOnSunday);
    }
    /**
     * Get OpenAfter1600 value
     * @return KaribooEnumYesNo|null
     */
    public function getOpenAfter1600()
    {
        return $this->OpenAfter1600;
    }
    /**
     * Set OpenAfter1600 value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_openAfter1600 the OpenAfter1600
     * @return KaribooEnumYesNo
     */
    public function setOpenAfter1600($_openAfter1600)
    {
        if(!KaribooEnumYesNo::valueIsValid($_openAfter1600))
        {
            return false;
        }
        return ($this->OpenAfter1600 = $_openAfter1600);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructSpotAroundMeRequest
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
