<?php
/**
 * File for class KaribooStructSpotRequest
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructSpotRequest originally named SpotRequest
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructSpotRequest extends KaribooWsdlClass
{
    /**
     * The shopID
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $shopID;
    /**
     * Constructor method for SpotRequest
     * @see parent::__construct()
     * @param string $_shopID
     * @return KaribooStructSpotRequest
     */
    public function __construct($_shopID = NULL)
    {
        parent::__construct(array('shopID'=>$_shopID),false);
    }
    /**
     * Get shopID value
     * @return string|null
     */
    public function getShopID()
    {
        return $this->shopID;
    }
    /**
     * Set shopID value
     * @param string $_shopID the shopID
     * @return string
     */
    public function setShopID($_shopID)
    {
        return ($this->shopID = $_shopID);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructSpotRequest
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
