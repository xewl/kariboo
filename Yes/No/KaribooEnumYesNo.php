<?php
/**
 * File for class KaribooEnumYesNo
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooEnumYesNo originally named yesNo
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooEnumYesNo extends KaribooWsdlClass
{
    /**
     * Constant for value 'Y'
     * @return string 'Y'
     */
    const VALUE_Y = 'Y';
    /**
     * Constant for value 'N'
     * @return string 'N'
     */
    const VALUE_N = 'N';
    /**
     * Return true if value is allowed
     * @uses KaribooEnumYesNo::VALUE_Y
     * @uses KaribooEnumYesNo::VALUE_N
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(KaribooEnumYesNo::VALUE_Y,KaribooEnumYesNo::VALUE_N));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
