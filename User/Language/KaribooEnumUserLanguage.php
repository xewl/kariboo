<?php
/**
 * File for class KaribooEnumUserLanguage
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooEnumUserLanguage originally named UserLanguage
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooEnumUserLanguage extends KaribooWsdlClass
{
    /**
     * Constant for value 'EN'
     * @return string 'EN'
     */
    const VALUE_EN = 'EN';
    /**
     * Constant for value 'FR'
     * @return string 'FR'
     */
    const VALUE_FR = 'FR';
    /**
     * Constant for value 'NL'
     * @return string 'NL'
     */
    const VALUE_NL = 'NL';
    /**
     * Return true if value is allowed
     * @uses KaribooEnumUserLanguage::VALUE_EN
     * @uses KaribooEnumUserLanguage::VALUE_FR
     * @uses KaribooEnumUserLanguage::VALUE_NL
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(KaribooEnumUserLanguage::VALUE_EN,KaribooEnumUserLanguage::VALUE_FR,KaribooEnumUserLanguage::VALUE_NL));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
