<?php
/**
 * File for class KaribooStructAnnounceRequest
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructAnnounceRequest originally named AnnounceRequest
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructAnnounceRequest extends KaribooWsdlClass
{
    /**
     * The AnnounceOrigin
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $AnnounceOrigin;
    /**
     * The OperationType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumOperationType
     */
    public $OperationType;
    /**
     * The Parcel
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructAnnounceParcel
     */
    public $Parcel;
    /**
     * The Address
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfAnnounceAddress
     */
    public $Address;
    /**
     * Constructor method for AnnounceRequest
     * @see parent::__construct()
     * @param string $_announceOrigin
     * @param KaribooEnumOperationType $_operationType
     * @param KaribooStructAnnounceParcel $_parcel
     * @param KaribooStructArrayOfAnnounceAddress $_address
     * @return KaribooStructAnnounceRequest
     */
    public function __construct($_announceOrigin = NULL,$_operationType = NULL,$_parcel = NULL,$_address = NULL)
    {
        parent::__construct(array('AnnounceOrigin'=>$_announceOrigin,'OperationType'=>$_operationType,'Parcel'=>$_parcel,'Address'=>($_address instanceof KaribooStructArrayOfAnnounceAddress)?$_address:new KaribooStructArrayOfAnnounceAddress($_address)),false);
    }
    /**
     * Get AnnounceOrigin value
     * @return string|null
     */
    public function getAnnounceOrigin()
    {
        return $this->AnnounceOrigin;
    }
    /**
     * Set AnnounceOrigin value
     * @param string $_announceOrigin the AnnounceOrigin
     * @return string
     */
    public function setAnnounceOrigin($_announceOrigin)
    {
        return ($this->AnnounceOrigin = $_announceOrigin);
    }
    /**
     * Get OperationType value
     * @return KaribooEnumOperationType|null
     */
    public function getOperationType()
    {
        return $this->OperationType;
    }
    /**
     * Set OperationType value
     * @uses KaribooEnumOperationType::valueIsValid()
     * @param KaribooEnumOperationType $_operationType the OperationType
     * @return KaribooEnumOperationType
     */
    public function setOperationType($_operationType)
    {
        if(!KaribooEnumOperationType::valueIsValid($_operationType))
        {
            return false;
        }
        return ($this->OperationType = $_operationType);
    }
    /**
     * Get Parcel value
     * @return KaribooStructAnnounceParcel|null
     */
    public function getParcel()
    {
        return $this->Parcel;
    }
    /**
     * Set Parcel value
     * @param KaribooStructAnnounceParcel $_parcel the Parcel
     * @return KaribooStructAnnounceParcel
     */
    public function setParcel($_parcel)
    {
        return ($this->Parcel = $_parcel);
    }
    /**
     * Get Address value
     * @return KaribooStructArrayOfAnnounceAddress|null
     */
    public function getAddress()
    {
        return $this->Address;
    }
    /**
     * Set Address value
     * @param KaribooStructArrayOfAnnounceAddress $_address the Address
     * @return KaribooStructArrayOfAnnounceAddress
     */
    public function setAddress($_address)
    {
        return ($this->Address = $_address);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructAnnounceRequest
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
