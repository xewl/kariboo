<?php
/**
 * File for class KaribooStructAnnounceResponse
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructAnnounceResponse originally named AnnounceResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructAnnounceResponse extends KaribooWsdlClass
{
    /**
     * The StatusCode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $StatusCode;
    /**
     * The Shipments
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfResponseShipments
     */
    public $Shipments;
    /**
     * The LogisticId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $LogisticId;
    /**
     * The SapId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $SapId;
    /**
     * The WareHouseId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $WareHouseId;
    /**
     * The RouteId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $RouteId;
    /**
     * The SortId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $SortId;
    /**
     * Constructor method for AnnounceResponse
     * @see parent::__construct()
     * @param string $_statusCode
     * @param KaribooStructArrayOfResponseShipments $_shipments
     * @param string $_logisticId
     * @param string $_sapId
     * @param string $_wareHouseId
     * @param string $_routeId
     * @param string $_sortId
     * @return KaribooStructAnnounceResponse
     */
    public function __construct($_statusCode = NULL,$_shipments = NULL,$_logisticId = NULL,$_sapId = NULL,$_wareHouseId = NULL,$_routeId = NULL,$_sortId = NULL)
    {
        parent::__construct(array('StatusCode'=>$_statusCode,'Shipments'=>($_shipments instanceof KaribooStructArrayOfResponseShipments)?$_shipments:new KaribooStructArrayOfResponseShipments($_shipments),'LogisticId'=>$_logisticId,'SapId'=>$_sapId,'WareHouseId'=>$_wareHouseId,'RouteId'=>$_routeId,'SortId'=>$_sortId),false);
    }
    /**
     * Get StatusCode value
     * @return string|null
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }
    /**
     * Set StatusCode value
     * @param string $_statusCode the StatusCode
     * @return string
     */
    public function setStatusCode($_statusCode)
    {
        return ($this->StatusCode = $_statusCode);
    }
    /**
     * Get Shipments value
     * @return KaribooStructArrayOfResponseShipments|null
     */
    public function getShipments()
    {
        return $this->Shipments;
    }
    /**
     * Set Shipments value
     * @param KaribooStructArrayOfResponseShipments $_shipments the Shipments
     * @return KaribooStructArrayOfResponseShipments
     */
    public function setShipments($_shipments)
    {
        return ($this->Shipments = $_shipments);
    }
    /**
     * Get LogisticId value
     * @return string|null
     */
    public function getLogisticId()
    {
        return $this->LogisticId;
    }
    /**
     * Set LogisticId value
     * @param string $_logisticId the LogisticId
     * @return string
     */
    public function setLogisticId($_logisticId)
    {
        return ($this->LogisticId = $_logisticId);
    }
    /**
     * Get SapId value
     * @return string|null
     */
    public function getSapId()
    {
        return $this->SapId;
    }
    /**
     * Set SapId value
     * @param string $_sapId the SapId
     * @return string
     */
    public function setSapId($_sapId)
    {
        return ($this->SapId = $_sapId);
    }
    /**
     * Get WareHouseId value
     * @return string|null
     */
    public function getWareHouseId()
    {
        return $this->WareHouseId;
    }
    /**
     * Set WareHouseId value
     * @param string $_wareHouseId the WareHouseId
     * @return string
     */
    public function setWareHouseId($_wareHouseId)
    {
        return ($this->WareHouseId = $_wareHouseId);
    }
    /**
     * Get RouteId value
     * @return string|null
     */
    public function getRouteId()
    {
        return $this->RouteId;
    }
    /**
     * Set RouteId value
     * @param string $_routeId the RouteId
     * @return string
     */
    public function setRouteId($_routeId)
    {
        return ($this->RouteId = $_routeId);
    }
    /**
     * Get SortId value
     * @return string|null
     */
    public function getSortId()
    {
        return $this->SortId;
    }
    /**
     * Set SortId value
     * @param string $_sortId the SortId
     * @return string
     */
    public function setSortId($_sortId)
    {
        return ($this->SortId = $_sortId);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructAnnounceResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
