<?php
/**
 * File for class KaribooStructAnnounceAddress
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructAnnounceAddress originally named AnnounceAddress
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructAnnounceAddress extends KaribooWsdlClass
{
    /**
     * The AddressType
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $AddressType;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Name;
    /**
     * The FirstName
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $FirstName;
    /**
     * The BuildingName
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $BuildingName;
    /**
     * The CompanyName
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $CompanyName;
    /**
     * The Departement
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Departement;
    /**
     * The Floor
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Floor;
    /**
     * The Street
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Street;
    /**
     * The StreetNumber
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $StreetNumber;
    /**
     * The Box
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Box;
    /**
     * The PostalCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $PostalCode;
    /**
     * The City
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $City;
    /**
     * The Country
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Country;
    /**
     * The Remark
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Remark;
    /**
     * The PhoneNumber
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $PhoneNumber;
    /**
     * The Email
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Email;
    /**
     * The Birthday
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Birthday;
    /**
     * The IDCardNo
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $IDCardNo;
    /**
     * The VAT
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $VAT;
    /**
     * The EnterpriseNo
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $EnterpriseNo;
    /**
     * The Notification
     * @var KaribooEnumYesNo
     */
    public $Notification;
    /**
     * Constructor method for AnnounceAddress
     * @see parent::__construct()
     * @param string $_addressType
     * @param string $_name
     * @param string $_firstName
     * @param string $_buildingName
     * @param string $_companyName
     * @param string $_departement
     * @param string $_floor
     * @param string $_street
     * @param string $_streetNumber
     * @param string $_box
     * @param string $_postalCode
     * @param string $_city
     * @param string $_country
     * @param string $_remark
     * @param string $_phoneNumber
     * @param string $_email
     * @param string $_birthday
     * @param string $_iDCardNo
     * @param string $_vAT
     * @param string $_enterpriseNo
     * @param KaribooEnumYesNo $_notification
     * @return KaribooStructAnnounceAddress
     */
    public function __construct($_addressType = NULL,$_name = NULL,$_firstName = NULL,$_buildingName = NULL,$_companyName = NULL,$_departement = NULL,$_floor = NULL,$_street = NULL,$_streetNumber = NULL,$_box = NULL,$_postalCode = NULL,$_city = NULL,$_country = NULL,$_remark = NULL,$_phoneNumber = NULL,$_email = NULL,$_birthday = NULL,$_iDCardNo = NULL,$_vAT = NULL,$_enterpriseNo = NULL,$_notification = NULL)
    {
        parent::__construct(array('AddressType'=>$_addressType,'Name'=>$_name,'FirstName'=>$_firstName,'BuildingName'=>$_buildingName,'CompanyName'=>$_companyName,'Departement'=>$_departement,'Floor'=>$_floor,'Street'=>$_street,'StreetNumber'=>$_streetNumber,'Box'=>$_box,'PostalCode'=>$_postalCode,'City'=>$_city,'Country'=>$_country,'Remark'=>$_remark,'PhoneNumber'=>$_phoneNumber,'Email'=>$_email,'Birthday'=>$_birthday,'IDCardNo'=>$_iDCardNo,'VAT'=>$_vAT,'EnterpriseNo'=>$_enterpriseNo,'Notification'=>$_notification),false);
    }
    /**
     * Get AddressType value
     * @return string|null
     */
    public function getAddressType()
    {
        return $this->AddressType;
    }
    /**
     * Set AddressType value
     * @param string $_addressType the AddressType
     * @return string
     */
    public function setAddressType($_addressType)
    {
        return ($this->AddressType = $_addressType);
    }
    /**
     * Get Name value
     * @return string|null
     */
    public function getName()
    {
        return $this->Name;
    }
    /**
     * Set Name value
     * @param string $_name the Name
     * @return string
     */
    public function setName($_name)
    {
        return ($this->Name = $_name);
    }
    /**
     * Get FirstName value
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }
    /**
     * Set FirstName value
     * @param string $_firstName the FirstName
     * @return string
     */
    public function setFirstName($_firstName)
    {
        return ($this->FirstName = $_firstName);
    }
    /**
     * Get BuildingName value
     * @return string|null
     */
    public function getBuildingName()
    {
        return $this->BuildingName;
    }
    /**
     * Set BuildingName value
     * @param string $_buildingName the BuildingName
     * @return string
     */
    public function setBuildingName($_buildingName)
    {
        return ($this->BuildingName = $_buildingName);
    }
    /**
     * Get CompanyName value
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->CompanyName;
    }
    /**
     * Set CompanyName value
     * @param string $_companyName the CompanyName
     * @return string
     */
    public function setCompanyName($_companyName)
    {
        return ($this->CompanyName = $_companyName);
    }
    /**
     * Get Departement value
     * @return string|null
     */
    public function getDepartement()
    {
        return $this->Departement;
    }
    /**
     * Set Departement value
     * @param string $_departement the Departement
     * @return string
     */
    public function setDepartement($_departement)
    {
        return ($this->Departement = $_departement);
    }
    /**
     * Get Floor value
     * @return string|null
     */
    public function getFloor()
    {
        return $this->Floor;
    }
    /**
     * Set Floor value
     * @param string $_floor the Floor
     * @return string
     */
    public function setFloor($_floor)
    {
        return ($this->Floor = $_floor);
    }
    /**
     * Get Street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->Street;
    }
    /**
     * Set Street value
     * @param string $_street the Street
     * @return string
     */
    public function setStreet($_street)
    {
        return ($this->Street = $_street);
    }
    /**
     * Get StreetNumber value
     * @return string|null
     */
    public function getStreetNumber()
    {
        return $this->StreetNumber;
    }
    /**
     * Set StreetNumber value
     * @param string $_streetNumber the StreetNumber
     * @return string
     */
    public function setStreetNumber($_streetNumber)
    {
        return ($this->StreetNumber = $_streetNumber);
    }
    /**
     * Get Box value
     * @return string|null
     */
    public function getBox()
    {
        return $this->Box;
    }
    /**
     * Set Box value
     * @param string $_box the Box
     * @return string
     */
    public function setBox($_box)
    {
        return ($this->Box = $_box);
    }
    /**
     * Get PostalCode value
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->PostalCode;
    }
    /**
     * Set PostalCode value
     * @param string $_postalCode the PostalCode
     * @return string
     */
    public function setPostalCode($_postalCode)
    {
        return ($this->PostalCode = $_postalCode);
    }
    /**
     * Get City value
     * @return string|null
     */
    public function getCity()
    {
        return $this->City;
    }
    /**
     * Set City value
     * @param string $_city the City
     * @return string
     */
    public function setCity($_city)
    {
        return ($this->City = $_city);
    }
    /**
     * Get Country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->Country;
    }
    /**
     * Set Country value
     * @param string $_country the Country
     * @return string
     */
    public function setCountry($_country)
    {
        return ($this->Country = $_country);
    }
    /**
     * Get Remark value
     * @return string|null
     */
    public function getRemark()
    {
        return $this->Remark;
    }
    /**
     * Set Remark value
     * @param string $_remark the Remark
     * @return string
     */
    public function setRemark($_remark)
    {
        return ($this->Remark = $_remark);
    }
    /**
     * Get PhoneNumber value
     * @return string|null
     */
    public function getPhoneNumber()
    {
        return $this->PhoneNumber;
    }
    /**
     * Set PhoneNumber value
     * @param string $_phoneNumber the PhoneNumber
     * @return string
     */
    public function setPhoneNumber($_phoneNumber)
    {
        return ($this->PhoneNumber = $_phoneNumber);
    }
    /**
     * Get Email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->Email;
    }
    /**
     * Set Email value
     * @param string $_email the Email
     * @return string
     */
    public function setEmail($_email)
    {
        return ($this->Email = $_email);
    }
    /**
     * Get Birthday value
     * @return string|null
     */
    public function getBirthday()
    {
        return $this->Birthday;
    }
    /**
     * Set Birthday value
     * @param string $_birthday the Birthday
     * @return string
     */
    public function setBirthday($_birthday)
    {
        return ($this->Birthday = $_birthday);
    }
    /**
     * Get IDCardNo value
     * @return string|null
     */
    public function getIDCardNo()
    {
        return $this->IDCardNo;
    }
    /**
     * Set IDCardNo value
     * @param string $_iDCardNo the IDCardNo
     * @return string
     */
    public function setIDCardNo($_iDCardNo)
    {
        return ($this->IDCardNo = $_iDCardNo);
    }
    /**
     * Get VAT value
     * @return string|null
     */
    public function getVAT()
    {
        return $this->VAT;
    }
    /**
     * Set VAT value
     * @param string $_vAT the VAT
     * @return string
     */
    public function setVAT($_vAT)
    {
        return ($this->VAT = $_vAT);
    }
    /**
     * Get EnterpriseNo value
     * @return string|null
     */
    public function getEnterpriseNo()
    {
        return $this->EnterpriseNo;
    }
    /**
     * Set EnterpriseNo value
     * @param string $_enterpriseNo the EnterpriseNo
     * @return string
     */
    public function setEnterpriseNo($_enterpriseNo)
    {
        return ($this->EnterpriseNo = $_enterpriseNo);
    }
    /**
     * Get Notification value
     * @return KaribooEnumYesNo|null
     */
    public function getNotification()
    {
        return $this->Notification;
    }
    /**
     * Set Notification value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_notification the Notification
     * @return KaribooEnumYesNo
     */
    public function setNotification($_notification)
    {
        if(!KaribooEnumYesNo::valueIsValid($_notification))
        {
            return false;
        }
        return ($this->Notification = $_notification);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructAnnounceAddress
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
