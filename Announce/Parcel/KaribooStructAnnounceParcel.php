<?php
/**
 * File for class KaribooStructAnnounceParcel
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructAnnounceParcel originally named AnnounceParcel
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructAnnounceParcel extends KaribooWsdlClass
{
    /**
     * The Barcode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Barcode;
    /**
     * The COD
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $COD;
    /**
     * The CODPrice
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var float
     */
    public $CODPrice;
    /**
     * The CheckOnShopIdActif
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $CheckOnShopIdActif;
    /**
     * The Currency
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $Currency;
    /**
     * The FlowType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $FlowType;
    /**
     * The HomeDeliveryCode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $HomeDeliveryCode;
    /**
     * The LabelData
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructLabelConfiguration
     */
    public $LabelData;
    /**
     * The Language
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumUserLanguage
     */
    public $Language;
    /**
     * The MultiParcelId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $MultiParcelId;
    /**
     * The OrderId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $OrderId;
    /**
     * The Products
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfProduct
     */
    public $Products;
    /**
     * The ReferredBarcode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $ReferredBarcode;
    /**
     * The Shipments
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfShipment
     */
    public $Shipments;
    /**
     * The ShopId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $ShopId;
    /**
     * Constructor method for AnnounceParcel
     * @see parent::__construct()
     * @param string $_barcode
     * @param KaribooEnumYesNo $_cOD
     * @param float $_cODPrice
     * @param KaribooEnumYesNo $_checkOnShopIdActif
     * @param string $_currency
     * @param string $_flowType
     * @param string $_homeDeliveryCode
     * @param KaribooStructLabelConfiguration $_labelData
     * @param KaribooEnumUserLanguage $_language
     * @param string $_multiParcelId
     * @param string $_orderId
     * @param KaribooStructArrayOfProduct $_products
     * @param string $_referredBarcode
     * @param KaribooStructArrayOfShipment $_shipments
     * @param string $_shopId
     * @return KaribooStructAnnounceParcel
     */
    public function __construct($_barcode = NULL,$_cOD = NULL,$_cODPrice = NULL,$_checkOnShopIdActif = NULL,$_currency = NULL,$_flowType = NULL,$_homeDeliveryCode = NULL,$_labelData = NULL,$_language = NULL,$_multiParcelId = NULL,$_orderId = NULL,$_products = NULL,$_referredBarcode = NULL,$_shipments = NULL,$_shopId = NULL)
    {
        parent::__construct(array('Barcode'=>$_barcode,'COD'=>$_cOD,'CODPrice'=>$_cODPrice,'CheckOnShopIdActif'=>$_checkOnShopIdActif,'Currency'=>$_currency,'FlowType'=>$_flowType,'HomeDeliveryCode'=>$_homeDeliveryCode,'LabelData'=>$_labelData,'Language'=>$_language,'MultiParcelId'=>$_multiParcelId,'OrderId'=>$_orderId,'Products'=>($_products instanceof KaribooStructArrayOfProduct)?$_products:new KaribooStructArrayOfProduct($_products),'ReferredBarcode'=>$_referredBarcode,'Shipments'=>($_shipments instanceof KaribooStructArrayOfShipment)?$_shipments:new KaribooStructArrayOfShipment($_shipments),'ShopId'=>$_shopId),false);
    }
    /**
     * Get Barcode value
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->Barcode;
    }
    /**
     * Set Barcode value
     * @param string $_barcode the Barcode
     * @return string
     */
    public function setBarcode($_barcode)
    {
        return ($this->Barcode = $_barcode);
    }
    /**
     * Get COD value
     * @return KaribooEnumYesNo|null
     */
    public function getCOD()
    {
        return $this->COD;
    }
    /**
     * Set COD value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_cOD the COD
     * @return KaribooEnumYesNo
     */
    public function setCOD($_cOD)
    {
        if(!KaribooEnumYesNo::valueIsValid($_cOD))
        {
            return false;
        }
        return ($this->COD = $_cOD);
    }
    /**
     * Get CODPrice value
     * @return float|null
     */
    public function getCODPrice()
    {
        return $this->CODPrice;
    }
    /**
     * Set CODPrice value
     * @param float $_cODPrice the CODPrice
     * @return float
     */
    public function setCODPrice($_cODPrice)
    {
        return ($this->CODPrice = $_cODPrice);
    }
    /**
     * Get CheckOnShopIdActif value
     * @return KaribooEnumYesNo|null
     */
    public function getCheckOnShopIdActif()
    {
        return $this->CheckOnShopIdActif;
    }
    /**
     * Set CheckOnShopIdActif value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_checkOnShopIdActif the CheckOnShopIdActif
     * @return KaribooEnumYesNo
     */
    public function setCheckOnShopIdActif($_checkOnShopIdActif)
    {
        if(!KaribooEnumYesNo::valueIsValid($_checkOnShopIdActif))
        {
            return false;
        }
        return ($this->CheckOnShopIdActif = $_checkOnShopIdActif);
    }
    /**
     * Get Currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->Currency;
    }
    /**
     * Set Currency value
     * @param string $_currency the Currency
     * @return string
     */
    public function setCurrency($_currency)
    {
        return ($this->Currency = $_currency);
    }
    /**
     * Get FlowType value
     * @return string|null
     */
    public function getFlowType()
    {
        return $this->FlowType;
    }
    /**
     * Set FlowType value
     * @param string $_flowType the FlowType
     * @return string
     */
    public function setFlowType($_flowType)
    {
        return ($this->FlowType = $_flowType);
    }
    /**
     * Get HomeDeliveryCode value
     * @return string|null
     */
    public function getHomeDeliveryCode()
    {
        return $this->HomeDeliveryCode;
    }
    /**
     * Set HomeDeliveryCode value
     * @param string $_homeDeliveryCode the HomeDeliveryCode
     * @return string
     */
    public function setHomeDeliveryCode($_homeDeliveryCode)
    {
        return ($this->HomeDeliveryCode = $_homeDeliveryCode);
    }
    /**
     * Get LabelData value
     * @return KaribooStructLabelConfiguration|null
     */
    public function getLabelData()
    {
        return $this->LabelData;
    }
    /**
     * Set LabelData value
     * @param KaribooStructLabelConfiguration $_labelData the LabelData
     * @return KaribooStructLabelConfiguration
     */
    public function setLabelData($_labelData)
    {
        return ($this->LabelData = $_labelData);
    }
    /**
     * Get Language value
     * @return KaribooEnumUserLanguage|null
     */
    public function getLanguage()
    {
        return $this->Language;
    }
    /**
     * Set Language value
     * @uses KaribooEnumUserLanguage::valueIsValid()
     * @param KaribooEnumUserLanguage $_language the Language
     * @return KaribooEnumUserLanguage
     */
    public function setLanguage($_language)
    {
        if(!KaribooEnumUserLanguage::valueIsValid($_language))
        {
            return false;
        }
        return ($this->Language = $_language);
    }
    /**
     * Get MultiParcelId value
     * @return string|null
     */
    public function getMultiParcelId()
    {
        return $this->MultiParcelId;
    }
    /**
     * Set MultiParcelId value
     * @param string $_multiParcelId the MultiParcelId
     * @return string
     */
    public function setMultiParcelId($_multiParcelId)
    {
        return ($this->MultiParcelId = $_multiParcelId);
    }
    /**
     * Get OrderId value
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->OrderId;
    }
    /**
     * Set OrderId value
     * @param string $_orderId the OrderId
     * @return string
     */
    public function setOrderId($_orderId)
    {
        return ($this->OrderId = $_orderId);
    }
    /**
     * Get Products value
     * @return KaribooStructArrayOfProduct|null
     */
    public function getProducts()
    {
        return $this->Products;
    }
    /**
     * Set Products value
     * @param KaribooStructArrayOfProduct $_products the Products
     * @return KaribooStructArrayOfProduct
     */
    public function setProducts($_products)
    {
        return ($this->Products = $_products);
    }
    /**
     * Get ReferredBarcode value
     * @return string|null
     */
    public function getReferredBarcode()
    {
        return $this->ReferredBarcode;
    }
    /**
     * Set ReferredBarcode value
     * @param string $_referredBarcode the ReferredBarcode
     * @return string
     */
    public function setReferredBarcode($_referredBarcode)
    {
        return ($this->ReferredBarcode = $_referredBarcode);
    }
    /**
     * Get Shipments value
     * @return KaribooStructArrayOfShipment|null
     */
    public function getShipments()
    {
        return $this->Shipments;
    }
    /**
     * Set Shipments value
     * @param KaribooStructArrayOfShipment $_shipments the Shipments
     * @return KaribooStructArrayOfShipment
     */
    public function setShipments($_shipments)
    {
        return ($this->Shipments = $_shipments);
    }
    /**
     * Get ShopId value
     * @return string|null
     */
    public function getShopId()
    {
        return $this->ShopId;
    }
    /**
     * Set ShopId value
     * @param string $_shopId the ShopId
     * @return string
     */
    public function setShopId($_shopId)
    {
        return ($this->ShopId = $_shopId);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructAnnounceParcel
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
