<?php
/**
 * File for class KaribooStructStatusDescriptions
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructStatusDescriptions originally named StatusDescriptions
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructStatusDescriptions extends KaribooWsdlClass
{
    /**
     * The FR
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $FR;
    /**
     * The NL
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $NL;
    /**
     * The EN
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $EN;
    /**
     * Constructor method for StatusDescriptions
     * @see parent::__construct()
     * @param string $_fR
     * @param string $_nL
     * @param string $_eN
     * @return KaribooStructStatusDescriptions
     */
    public function __construct($_fR = NULL,$_nL = NULL,$_eN = NULL)
    {
        parent::__construct(array('FR'=>$_fR,'NL'=>$_nL,'EN'=>$_eN),false);
    }
    /**
     * Get FR value
     * @return string|null
     */
    public function getFR()
    {
        return $this->FR;
    }
    /**
     * Set FR value
     * @param string $_fR the FR
     * @return string
     */
    public function setFR($_fR)
    {
        return ($this->FR = $_fR);
    }
    /**
     * Get NL value
     * @return string|null
     */
    public function getNL()
    {
        return $this->NL;
    }
    /**
     * Set NL value
     * @param string $_nL the NL
     * @return string
     */
    public function setNL($_nL)
    {
        return ($this->NL = $_nL);
    }
    /**
     * Get EN value
     * @return string|null
     */
    public function getEN()
    {
        return $this->EN;
    }
    /**
     * Set EN value
     * @param string $_eN the EN
     * @return string
     */
    public function setEN($_eN)
    {
        return ($this->EN = $_eN);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructStatusDescriptions
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
