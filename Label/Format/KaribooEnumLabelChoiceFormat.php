<?php
/**
 * File for class KaribooEnumLabelChoiceFormat
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooEnumLabelChoiceFormat originally named labelChoiceFormat
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooEnumLabelChoiceFormat extends KaribooWsdlClass
{
    /**
     * Constant for value 'A5'
     * @return string 'A5'
     */
    const VALUE_A5 = 'A5';
    /**
     * Constant for value 'A6'
     * @return string 'A6'
     */
    const VALUE_A6 = 'A6';
    /**
     * Return true if value is allowed
     * @uses KaribooEnumLabelChoiceFormat::VALUE_A5
     * @uses KaribooEnumLabelChoiceFormat::VALUE_A6
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(KaribooEnumLabelChoiceFormat::VALUE_A5,KaribooEnumLabelChoiceFormat::VALUE_A6));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
