<?php
/**
 * File for class KaribooEnumLabelChoiceType
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooEnumLabelChoiceType originally named labelChoiceType
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooEnumLabelChoiceType extends KaribooWsdlClass
{
    /**
     * Constant for value 'PDF'
     * @return string 'PDF'
     */
    const VALUE_PDF = 'PDF';
    /**
     * Constant for value 'JPEG'
     * @return string 'JPEG'
     */
    const VALUE_JPEG = 'JPEG';
    /**
     * Constant for value 'PNG'
     * @return string 'PNG'
     */
    const VALUE_PNG = 'PNG';
    /**
     * Constant for value 'ZPL'
     * @return string 'ZPL'
     */
    const VALUE_ZPL = 'ZPL';
    /**
     * Return true if value is allowed
     * @uses KaribooEnumLabelChoiceType::VALUE_PDF
     * @uses KaribooEnumLabelChoiceType::VALUE_JPEG
     * @uses KaribooEnumLabelChoiceType::VALUE_PNG
     * @uses KaribooEnumLabelChoiceType::VALUE_ZPL
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(KaribooEnumLabelChoiceType::VALUE_PDF,KaribooEnumLabelChoiceType::VALUE_JPEG,KaribooEnumLabelChoiceType::VALUE_PNG,KaribooEnumLabelChoiceType::VALUE_ZPL));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
