<?php
/**
 * File for class KaribooStructLabelConfiguration
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructLabelConfiguration originally named LabelConfiguration
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructLabelConfiguration extends KaribooWsdlClass
{
    /**
     * The Choice
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $Choice;
    /**
     * The ChoiceFormat
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumLabelChoiceFormat
     */
    public $ChoiceFormat;
    /**
     * The ChoiceType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumLabelChoiceType
     */
    public $ChoiceType;
    /**
     * The DoNotReturnPDF
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $DoNotReturnPDF;
    /**
     * Constructor method for LabelConfiguration
     * @see parent::__construct()
     * @param KaribooEnumYesNo $_choice
     * @param KaribooEnumLabelChoiceFormat $_choiceFormat
     * @param KaribooEnumLabelChoiceType $_choiceType
     * @param KaribooEnumYesNo $_doNotReturnPDF
     * @return KaribooStructLabelConfiguration
     */
    public function __construct($_choice = NULL,$_choiceFormat = NULL,$_choiceType = NULL,$_doNotReturnPDF = NULL)
    {
        parent::__construct(array('Choice'=>$_choice,'ChoiceFormat'=>$_choiceFormat,'ChoiceType'=>$_choiceType,'DoNotReturnPDF'=>$_doNotReturnPDF),false);
    }
    /**
     * Get Choice value
     * @return KaribooEnumYesNo|null
     */
    public function getChoice()
    {
        return $this->Choice;
    }
    /**
     * Set Choice value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_choice the Choice
     * @return KaribooEnumYesNo
     */
    public function setChoice($_choice)
    {
        if(!KaribooEnumYesNo::valueIsValid($_choice))
        {
            return false;
        }
        return ($this->Choice = $_choice);
    }
    /**
     * Get ChoiceFormat value
     * @return KaribooEnumLabelChoiceFormat|null
     */
    public function getChoiceFormat()
    {
        return $this->ChoiceFormat;
    }
    /**
     * Set ChoiceFormat value
     * @uses KaribooEnumLabelChoiceFormat::valueIsValid()
     * @param KaribooEnumLabelChoiceFormat $_choiceFormat the ChoiceFormat
     * @return KaribooEnumLabelChoiceFormat
     */
    public function setChoiceFormat($_choiceFormat)
    {
        if(!KaribooEnumLabelChoiceFormat::valueIsValid($_choiceFormat))
        {
            return false;
        }
        return ($this->ChoiceFormat = $_choiceFormat);
    }
    /**
     * Get ChoiceType value
     * @return KaribooEnumLabelChoiceType|null
     */
    public function getChoiceType()
    {
        return $this->ChoiceType;
    }
    /**
     * Set ChoiceType value
     * @uses KaribooEnumLabelChoiceType::valueIsValid()
     * @param KaribooEnumLabelChoiceType $_choiceType the ChoiceType
     * @return KaribooEnumLabelChoiceType
     */
    public function setChoiceType($_choiceType)
    {
        if(!KaribooEnumLabelChoiceType::valueIsValid($_choiceType))
        {
            return false;
        }
        return ($this->ChoiceType = $_choiceType);
    }
    /**
     * Get DoNotReturnPDF value
     * @return KaribooEnumYesNo|null
     */
    public function getDoNotReturnPDF()
    {
        return $this->DoNotReturnPDF;
    }
    /**
     * Set DoNotReturnPDF value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_doNotReturnPDF the DoNotReturnPDF
     * @return KaribooEnumYesNo
     */
    public function setDoNotReturnPDF($_doNotReturnPDF)
    {
        if(!KaribooEnumYesNo::valueIsValid($_doNotReturnPDF))
        {
            return false;
        }
        return ($this->DoNotReturnPDF = $_doNotReturnPDF);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructLabelConfiguration
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
