<?php
/**
 * File for class KaribooStructErrors
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructErrors originally named Errors
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd3}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructErrors extends KaribooWsdlClass
{
    /**
     * The Description
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Description;
    /**
     * The ErrorMsg
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ErrorMsg;
    /**
     * The ErrorNumber
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ErrorNumber;
    /**
     * Constructor method for Errors
     * @see parent::__construct()
     * @param string $_description
     * @param string $_errorMsg
     * @param string $_errorNumber
     * @return KaribooStructErrors
     */
    public function __construct($_description = NULL,$_errorMsg = NULL,$_errorNumber = NULL)
    {
        parent::__construct(array('Description'=>$_description,'ErrorMsg'=>$_errorMsg,'ErrorNumber'=>$_errorNumber),false);
    }
    /**
     * Get Description value
     * @return string|null
     */
    public function getDescription()
    {
        return $this->Description;
    }
    /**
     * Set Description value
     * @param string $_description the Description
     * @return string
     */
    public function setDescription($_description)
    {
        return ($this->Description = $_description);
    }
    /**
     * Get ErrorMsg value
     * @return string|null
     */
    public function getErrorMsg()
    {
        return $this->ErrorMsg;
    }
    /**
     * Set ErrorMsg value
     * @param string $_errorMsg the ErrorMsg
     * @return string
     */
    public function setErrorMsg($_errorMsg)
    {
        return ($this->ErrorMsg = $_errorMsg);
    }
    /**
     * Get ErrorNumber value
     * @return string|null
     */
    public function getErrorNumber()
    {
        return $this->ErrorNumber;
    }
    /**
     * Set ErrorNumber value
     * @param string $_errorNumber the ErrorNumber
     * @return string
     */
    public function setErrorNumber($_errorNumber)
    {
        return ($this->ErrorNumber = $_errorNumber);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructErrors
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
