<?php
/**
 * File for class KaribooStructTracking
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructTracking originally named Tracking
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructTracking extends KaribooWsdlClass
{
    /**
     * The ErrorCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ErrorCode;
    /**
     * The Barcode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Barcode;
    /**
     * The FlowType
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $FlowType;
    /**
     * The OrderID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $OrderID;
    /**
     * The Receiver
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructTrackingContactInformations
     */
    public $Receiver;
    /**
     * The Sender
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructTrackingContactInformations
     */
    public $Sender;
    /**
     * The Delivery
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructTrackingContactInformations
     */
    public $Delivery;
    /**
     * The HistoryStatusList
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructArrayOfHistoryStatus
     */
    public $HistoryStatusList;
    /**
     * Constructor method for Tracking
     * @see parent::__construct()
     * @param string $_errorCode
     * @param string $_barcode
     * @param string $_flowType
     * @param string $_orderID
     * @param KaribooStructTrackingContactInformations $_receiver
     * @param KaribooStructTrackingContactInformations $_sender
     * @param KaribooStructTrackingContactInformations $_delivery
     * @param KaribooStructArrayOfHistoryStatus $_historyStatusList
     * @return KaribooStructTracking
     */
    public function __construct($_errorCode = NULL,$_barcode = NULL,$_flowType = NULL,$_orderID = NULL,$_receiver = NULL,$_sender = NULL,$_delivery = NULL,$_historyStatusList = NULL)
    {
        parent::__construct(array('ErrorCode'=>$_errorCode,'Barcode'=>$_barcode,'FlowType'=>$_flowType,'OrderID'=>$_orderID,'Receiver'=>$_receiver,'Sender'=>$_sender,'Delivery'=>$_delivery,'HistoryStatusList'=>($_historyStatusList instanceof KaribooStructArrayOfHistoryStatus)?$_historyStatusList:new KaribooStructArrayOfHistoryStatus($_historyStatusList)),false);
    }
    /**
     * Get ErrorCode value
     * @return string|null
     */
    public function getErrorCode()
    {
        return $this->ErrorCode;
    }
    /**
     * Set ErrorCode value
     * @param string $_errorCode the ErrorCode
     * @return string
     */
    public function setErrorCode($_errorCode)
    {
        return ($this->ErrorCode = $_errorCode);
    }
    /**
     * Get Barcode value
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->Barcode;
    }
    /**
     * Set Barcode value
     * @param string $_barcode the Barcode
     * @return string
     */
    public function setBarcode($_barcode)
    {
        return ($this->Barcode = $_barcode);
    }
    /**
     * Get FlowType value
     * @return string|null
     */
    public function getFlowType()
    {
        return $this->FlowType;
    }
    /**
     * Set FlowType value
     * @param string $_flowType the FlowType
     * @return string
     */
    public function setFlowType($_flowType)
    {
        return ($this->FlowType = $_flowType);
    }
    /**
     * Get OrderID value
     * @return string|null
     */
    public function getOrderID()
    {
        return $this->OrderID;
    }
    /**
     * Set OrderID value
     * @param string $_orderID the OrderID
     * @return string
     */
    public function setOrderID($_orderID)
    {
        return ($this->OrderID = $_orderID);
    }
    /**
     * Get Receiver value
     * @return KaribooStructTrackingContactInformations|null
     */
    public function getReceiver()
    {
        return $this->Receiver;
    }
    /**
     * Set Receiver value
     * @param KaribooStructTrackingContactInformations $_receiver the Receiver
     * @return KaribooStructTrackingContactInformations
     */
    public function setReceiver($_receiver)
    {
        return ($this->Receiver = $_receiver);
    }
    /**
     * Get Sender value
     * @return KaribooStructTrackingContactInformations|null
     */
    public function getSender()
    {
        return $this->Sender;
    }
    /**
     * Set Sender value
     * @param KaribooStructTrackingContactInformations $_sender the Sender
     * @return KaribooStructTrackingContactInformations
     */
    public function setSender($_sender)
    {
        return ($this->Sender = $_sender);
    }
    /**
     * Get Delivery value
     * @return KaribooStructTrackingContactInformations|null
     */
    public function getDelivery()
    {
        return $this->Delivery;
    }
    /**
     * Set Delivery value
     * @param KaribooStructTrackingContactInformations $_delivery the Delivery
     * @return KaribooStructTrackingContactInformations
     */
    public function setDelivery($_delivery)
    {
        return ($this->Delivery = $_delivery);
    }
    /**
     * Get HistoryStatusList value
     * @return KaribooStructArrayOfHistoryStatus|null
     */
    public function getHistoryStatusList()
    {
        return $this->HistoryStatusList;
    }
    /**
     * Set HistoryStatusList value
     * @param KaribooStructArrayOfHistoryStatus $_historyStatusList the HistoryStatusList
     * @return KaribooStructArrayOfHistoryStatus
     */
    public function setHistoryStatusList($_historyStatusList)
    {
        return ($this->HistoryStatusList = $_historyStatusList);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructTracking
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
