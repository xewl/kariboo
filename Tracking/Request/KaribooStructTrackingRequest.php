<?php
/**
 * File for class KaribooStructTrackingRequest
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructTrackingRequest originally named TrackingRequest
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructTrackingRequest extends KaribooWsdlClass
{
    /**
     * The TrackingSearch
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfSearchCriteria
     */
    public $TrackingSearch;
    /**
     * Constructor method for TrackingRequest
     * @see parent::__construct()
     * @param KaribooStructArrayOfSearchCriteria $_trackingSearch
     * @return KaribooStructTrackingRequest
     */
    public function __construct($_trackingSearch = NULL)
    {
        parent::__construct(array('TrackingSearch'=>($_trackingSearch instanceof KaribooStructArrayOfSearchCriteria)?$_trackingSearch:new KaribooStructArrayOfSearchCriteria($_trackingSearch)),false);
    }
    /**
     * Get TrackingSearch value
     * @return KaribooStructArrayOfSearchCriteria|null
     */
    public function getTrackingSearch()
    {
        return $this->TrackingSearch;
    }
    /**
     * Set TrackingSearch value
     * @param KaribooStructArrayOfSearchCriteria $_trackingSearch the TrackingSearch
     * @return KaribooStructArrayOfSearchCriteria
     */
    public function setTrackingSearch($_trackingSearch)
    {
        return ($this->TrackingSearch = $_trackingSearch);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructTrackingRequest
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
