<?php
/**
 * File for class KaribooStructTrackingContactInformations
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructTrackingContactInformations originally named TrackingContactInformations
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructTrackingContactInformations extends KaribooWsdlClass
{
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Name;
    /**
     * The Street
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Street;
    /**
     * The StreetNumber
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $StreetNumber;
    /**
     * The Box
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Box;
    /**
     * The PostalCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $PostalCode;
    /**
     * The City
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $City;
    /**
     * The Country
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Country;
    /**
     * The Phone
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Phone;
    /**
     * The Email
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Email;
    /**
     * The Weblink
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Weblink;
    /**
     * The ShopId
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopId;
    /**
     * Constructor method for TrackingContactInformations
     * @see parent::__construct()
     * @param string $_name
     * @param string $_street
     * @param string $_streetNumber
     * @param string $_box
     * @param string $_postalCode
     * @param string $_city
     * @param string $_country
     * @param string $_phone
     * @param string $_email
     * @param string $_weblink
     * @param string $_shopId
     * @return KaribooStructTrackingContactInformations
     */
    public function __construct($_name = NULL,$_street = NULL,$_streetNumber = NULL,$_box = NULL,$_postalCode = NULL,$_city = NULL,$_country = NULL,$_phone = NULL,$_email = NULL,$_weblink = NULL,$_shopId = NULL)
    {
        parent::__construct(array('Name'=>$_name,'Street'=>$_street,'StreetNumber'=>$_streetNumber,'Box'=>$_box,'PostalCode'=>$_postalCode,'City'=>$_city,'Country'=>$_country,'Phone'=>$_phone,'Email'=>$_email,'Weblink'=>$_weblink,'ShopId'=>$_shopId),false);
    }
    /**
     * Get Name value
     * @return string|null
     */
    public function getName()
    {
        return $this->Name;
    }
    /**
     * Set Name value
     * @param string $_name the Name
     * @return string
     */
    public function setName($_name)
    {
        return ($this->Name = $_name);
    }
    /**
     * Get Street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->Street;
    }
    /**
     * Set Street value
     * @param string $_street the Street
     * @return string
     */
    public function setStreet($_street)
    {
        return ($this->Street = $_street);
    }
    /**
     * Get StreetNumber value
     * @return string|null
     */
    public function getStreetNumber()
    {
        return $this->StreetNumber;
    }
    /**
     * Set StreetNumber value
     * @param string $_streetNumber the StreetNumber
     * @return string
     */
    public function setStreetNumber($_streetNumber)
    {
        return ($this->StreetNumber = $_streetNumber);
    }
    /**
     * Get Box value
     * @return string|null
     */
    public function getBox()
    {
        return $this->Box;
    }
    /**
     * Set Box value
     * @param string $_box the Box
     * @return string
     */
    public function setBox($_box)
    {
        return ($this->Box = $_box);
    }
    /**
     * Get PostalCode value
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->PostalCode;
    }
    /**
     * Set PostalCode value
     * @param string $_postalCode the PostalCode
     * @return string
     */
    public function setPostalCode($_postalCode)
    {
        return ($this->PostalCode = $_postalCode);
    }
    /**
     * Get City value
     * @return string|null
     */
    public function getCity()
    {
        return $this->City;
    }
    /**
     * Set City value
     * @param string $_city the City
     * @return string
     */
    public function setCity($_city)
    {
        return ($this->City = $_city);
    }
    /**
     * Get Country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->Country;
    }
    /**
     * Set Country value
     * @param string $_country the Country
     * @return string
     */
    public function setCountry($_country)
    {
        return ($this->Country = $_country);
    }
    /**
     * Get Phone value
     * @return string|null
     */
    public function getPhone()
    {
        return $this->Phone;
    }
    /**
     * Set Phone value
     * @param string $_phone the Phone
     * @return string
     */
    public function setPhone($_phone)
    {
        return ($this->Phone = $_phone);
    }
    /**
     * Get Email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->Email;
    }
    /**
     * Set Email value
     * @param string $_email the Email
     * @return string
     */
    public function setEmail($_email)
    {
        return ($this->Email = $_email);
    }
    /**
     * Get Weblink value
     * @return string|null
     */
    public function getWeblink()
    {
        return $this->Weblink;
    }
    /**
     * Set Weblink value
     * @param string $_weblink the Weblink
     * @return string
     */
    public function setWeblink($_weblink)
    {
        return ($this->Weblink = $_weblink);
    }
    /**
     * Get ShopId value
     * @return string|null
     */
    public function getShopId()
    {
        return $this->ShopId;
    }
    /**
     * Set ShopId value
     * @param string $_shopId the ShopId
     * @return string
     */
    public function setShopId($_shopId)
    {
        return ($this->ShopId = $_shopId);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructTrackingContactInformations
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
