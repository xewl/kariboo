<?php
/**
 * File for class KaribooStructTrackingResponse
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructTrackingResponse originally named TrackingResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructTrackingResponse extends KaribooWsdlClass
{
    /**
     * The TrackingResult
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfTracking
     */
    public $TrackingResult;
    /**
     * Constructor method for TrackingResponse
     * @see parent::__construct()
     * @param KaribooStructArrayOfTracking $_trackingResult
     * @return KaribooStructTrackingResponse
     */
    public function __construct($_trackingResult = NULL)
    {
        parent::__construct(array('TrackingResult'=>($_trackingResult instanceof KaribooStructArrayOfTracking)?$_trackingResult:new KaribooStructArrayOfTracking($_trackingResult)),false);
    }
    /**
     * Get TrackingResult value
     * @return KaribooStructArrayOfTracking|null
     */
    public function getTrackingResult()
    {
        return $this->TrackingResult;
    }
    /**
     * Set TrackingResult value
     * @param KaribooStructArrayOfTracking $_trackingResult the TrackingResult
     * @return KaribooStructArrayOfTracking
     */
    public function setTrackingResult($_trackingResult)
    {
        return ($this->TrackingResult = $_trackingResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructTrackingResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
