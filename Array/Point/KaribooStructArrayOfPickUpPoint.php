<?php
/**
 * File for class KaribooStructArrayOfPickUpPoint
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfPickUpPoint originally named ArrayOfPickUpPoint
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfPickUpPoint extends KaribooWsdlClass
{
    /**
     * The PickUpPoint
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructPickUpPoint
     */
    public $PickUpPoint;
    /**
     * Constructor method for ArrayOfPickUpPoint
     * @see parent::__construct()
     * @param KaribooStructPickUpPoint $_pickUpPoint
     * @return KaribooStructArrayOfPickUpPoint
     */
    public function __construct($_pickUpPoint = NULL)
    {
        parent::__construct(array('PickUpPoint'=>$_pickUpPoint),false);
    }
    /**
     * Get PickUpPoint value
     * @return KaribooStructPickUpPoint|null
     */
    public function getPickUpPoint()
    {
        return $this->PickUpPoint;
    }
    /**
     * Set PickUpPoint value
     * @param KaribooStructPickUpPoint $_pickUpPoint the PickUpPoint
     * @return KaribooStructPickUpPoint
     */
    public function setPickUpPoint($_pickUpPoint)
    {
        return ($this->PickUpPoint = $_pickUpPoint);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructPickUpPoint
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructPickUpPoint
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructPickUpPoint
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructPickUpPoint
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructPickUpPoint
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string PickUpPoint
     */
    public function getAttributeName()
    {
        return 'PickUpPoint';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfPickUpPoint
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
