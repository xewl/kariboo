<?php
/**
 * File for class KaribooStructArrayOfSearchCriteria
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfSearchCriteria originally named ArrayOfSearchCriteria
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfSearchCriteria extends KaribooWsdlClass
{
    /**
     * The SearchCriteria
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructSearchCriteria
     */
    public $SearchCriteria;
    /**
     * Constructor method for ArrayOfSearchCriteria
     * @see parent::__construct()
     * @param KaribooStructSearchCriteria $_searchCriteria
     * @return KaribooStructArrayOfSearchCriteria
     */
    public function __construct($_searchCriteria = NULL)
    {
        parent::__construct(array('SearchCriteria'=>$_searchCriteria),false);
    }
    /**
     * Get SearchCriteria value
     * @return KaribooStructSearchCriteria|null
     */
    public function getSearchCriteria()
    {
        return $this->SearchCriteria;
    }
    /**
     * Set SearchCriteria value
     * @param KaribooStructSearchCriteria $_searchCriteria the SearchCriteria
     * @return KaribooStructSearchCriteria
     */
    public function setSearchCriteria($_searchCriteria)
    {
        return ($this->SearchCriteria = $_searchCriteria);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructSearchCriteria
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructSearchCriteria
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructSearchCriteria
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructSearchCriteria
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructSearchCriteria
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string SearchCriteria
     */
    public function getAttributeName()
    {
        return 'SearchCriteria';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfSearchCriteria
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
