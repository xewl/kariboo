<?php
/**
 * File for class KaribooStructArrayOfInvoiceDetails
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfInvoiceDetails originally named ArrayOfInvoiceDetails
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfInvoiceDetails extends KaribooWsdlClass
{
    /**
     * The DetailsLines
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructFinancialInvoiceDetail
     */
    public $DetailsLines;
    /**
     * Constructor method for ArrayOfInvoiceDetails
     * @see parent::__construct()
     * @param KaribooStructFinancialInvoiceDetail $_detailsLines
     * @return KaribooStructArrayOfInvoiceDetails
     */
    public function __construct($_detailsLines = NULL)
    {
        parent::__construct(array('DetailsLines'=>$_detailsLines),false);
    }
    /**
     * Get DetailsLines value
     * @return KaribooStructFinancialInvoiceDetail|null
     */
    public function getDetailsLines()
    {
        return $this->DetailsLines;
    }
    /**
     * Set DetailsLines value
     * @param KaribooStructFinancialInvoiceDetail $_detailsLines the DetailsLines
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function setDetailsLines($_detailsLines)
    {
        return ($this->DetailsLines = $_detailsLines);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string DetailsLines
     */
    public function getAttributeName()
    {
        return 'DetailsLines';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfInvoiceDetails
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
