<?php
/**
 * File for class KaribooStructArrayOfHistoryStatus
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfHistoryStatus originally named ArrayOfHistoryStatus
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfHistoryStatus extends KaribooWsdlClass
{
    /**
     * The HistoryStatus
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructHistoryStatus
     */
    public $HistoryStatus;
    /**
     * Constructor method for ArrayOfHistoryStatus
     * @see parent::__construct()
     * @param KaribooStructHistoryStatus $_historyStatus
     * @return KaribooStructArrayOfHistoryStatus
     */
    public function __construct($_historyStatus = NULL)
    {
        parent::__construct(array('HistoryStatus'=>$_historyStatus),false);
    }
    /**
     * Get HistoryStatus value
     * @return KaribooStructHistoryStatus|null
     */
    public function getHistoryStatus()
    {
        return $this->HistoryStatus;
    }
    /**
     * Set HistoryStatus value
     * @param KaribooStructHistoryStatus $_historyStatus the HistoryStatus
     * @return KaribooStructHistoryStatus
     */
    public function setHistoryStatus($_historyStatus)
    {
        return ($this->HistoryStatus = $_historyStatus);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructHistoryStatus
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructHistoryStatus
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructHistoryStatus
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructHistoryStatus
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructHistoryStatus
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string HistoryStatus
     */
    public function getAttributeName()
    {
        return 'HistoryStatus';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfHistoryStatus
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
