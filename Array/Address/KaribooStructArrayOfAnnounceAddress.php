<?php
/**
 * File for class KaribooStructArrayOfAnnounceAddress
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfAnnounceAddress originally named ArrayOfAnnounceAddress
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfAnnounceAddress extends KaribooWsdlClass
{
    /**
     * The AnnounceAddress
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructAnnounceAddress
     */
    public $AnnounceAddress;
    /**
     * Constructor method for ArrayOfAnnounceAddress
     * @see parent::__construct()
     * @param KaribooStructAnnounceAddress $_announceAddress
     * @return KaribooStructArrayOfAnnounceAddress
     */
    public function __construct($_announceAddress = NULL)
    {
        parent::__construct(array('AnnounceAddress'=>$_announceAddress),false);
    }
    /**
     * Get AnnounceAddress value
     * @return KaribooStructAnnounceAddress|null
     */
    public function getAnnounceAddress()
    {
        return $this->AnnounceAddress;
    }
    /**
     * Set AnnounceAddress value
     * @param KaribooStructAnnounceAddress $_announceAddress the AnnounceAddress
     * @return KaribooStructAnnounceAddress
     */
    public function setAnnounceAddress($_announceAddress)
    {
        return ($this->AnnounceAddress = $_announceAddress);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructAnnounceAddress
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructAnnounceAddress
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructAnnounceAddress
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructAnnounceAddress
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructAnnounceAddress
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string AnnounceAddress
     */
    public function getAttributeName()
    {
        return 'AnnounceAddress';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfAnnounceAddress
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
