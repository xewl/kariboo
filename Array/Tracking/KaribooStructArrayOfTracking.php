<?php
/**
 * File for class KaribooStructArrayOfTracking
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfTracking originally named ArrayOfTracking
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfTracking extends KaribooWsdlClass
{
    /**
     * The Tracking
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructTracking
     */
    public $Tracking;
    /**
     * Constructor method for ArrayOfTracking
     * @see parent::__construct()
     * @param KaribooStructTracking $_tracking
     * @return KaribooStructArrayOfTracking
     */
    public function __construct($_tracking = NULL)
    {
        parent::__construct(array('Tracking'=>$_tracking),false);
    }
    /**
     * Get Tracking value
     * @return KaribooStructTracking|null
     */
    public function getTracking()
    {
        return $this->Tracking;
    }
    /**
     * Set Tracking value
     * @param KaribooStructTracking $_tracking the Tracking
     * @return KaribooStructTracking
     */
    public function setTracking($_tracking)
    {
        return ($this->Tracking = $_tracking);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructTracking
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructTracking
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructTracking
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructTracking
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructTracking
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string Tracking
     */
    public function getAttributeName()
    {
        return 'Tracking';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfTracking
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
