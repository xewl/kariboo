<?php
/**
 * File for class KaribooStructArrayOfResponseShipments
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructArrayOfResponseShipments originally named ArrayOfResponseShipments
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructArrayOfResponseShipments extends KaribooWsdlClass
{
    /**
     * The ResponseShipments
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructResponseShipments
     */
    public $ResponseShipments;
    /**
     * Constructor method for ArrayOfResponseShipments
     * @see parent::__construct()
     * @param KaribooStructResponseShipments $_responseShipments
     * @return KaribooStructArrayOfResponseShipments
     */
    public function __construct($_responseShipments = NULL)
    {
        parent::__construct(array('ResponseShipments'=>$_responseShipments),false);
    }
    /**
     * Get ResponseShipments value
     * @return KaribooStructResponseShipments|null
     */
    public function getResponseShipments()
    {
        return $this->ResponseShipments;
    }
    /**
     * Set ResponseShipments value
     * @param KaribooStructResponseShipments $_responseShipments the ResponseShipments
     * @return KaribooStructResponseShipments
     */
    public function setResponseShipments($_responseShipments)
    {
        return ($this->ResponseShipments = $_responseShipments);
    }
    /**
     * Returns the current element
     * @see KaribooWsdlClass::current()
     * @return KaribooStructResponseShipments
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see KaribooWsdlClass::item()
     * @param int $_index
     * @return KaribooStructResponseShipments
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see KaribooWsdlClass::first()
     * @return KaribooStructResponseShipments
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see KaribooWsdlClass::last()
     * @return KaribooStructResponseShipments
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see KaribooWsdlClass::last()
     * @param int $_offset
     * @return KaribooStructResponseShipments
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see KaribooWsdlClass::getAttributeName()
     * @return string ResponseShipments
     */
    public function getAttributeName()
    {
        return 'ResponseShipments';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructArrayOfResponseShipments
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
