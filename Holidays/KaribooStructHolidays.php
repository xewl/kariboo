<?php
/**
 * File for class KaribooStructHolidays
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructHolidays originally named Holidays
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructHolidays extends KaribooWsdlClass
{
    /**
     * The DateCD1
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCD1;
    /**
     * The DateCF1
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCF1;
    /**
     * The DateCD2
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCD2;
    /**
     * The DateCF2
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCF2;
    /**
     * The DateCD3
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCD3;
    /**
     * The DateCF3
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCF3;
    /**
     * The DateCD4
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCD4;
    /**
     * The DateCF4
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateCF4;
    /**
     * The DateLO1
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateLO1;
    /**
     * The DateLO2
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateLO2;
    /**
     * The DateLO3
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateLO3;
    /**
     * The DateLO4
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $DateLO4;
    /**
     * Constructor method for Holidays
     * @see parent::__construct()
     * @param string $_dateCD1
     * @param string $_dateCF1
     * @param string $_dateCD2
     * @param string $_dateCF2
     * @param string $_dateCD3
     * @param string $_dateCF3
     * @param string $_dateCD4
     * @param string $_dateCF4
     * @param string $_dateLO1
     * @param string $_dateLO2
     * @param string $_dateLO3
     * @param string $_dateLO4
     * @return KaribooStructHolidays
     */
    public function __construct($_dateCD1 = NULL,$_dateCF1 = NULL,$_dateCD2 = NULL,$_dateCF2 = NULL,$_dateCD3 = NULL,$_dateCF3 = NULL,$_dateCD4 = NULL,$_dateCF4 = NULL,$_dateLO1 = NULL,$_dateLO2 = NULL,$_dateLO3 = NULL,$_dateLO4 = NULL)
    {
        parent::__construct(array('DateCD1'=>$_dateCD1,'DateCF1'=>$_dateCF1,'DateCD2'=>$_dateCD2,'DateCF2'=>$_dateCF2,'DateCD3'=>$_dateCD3,'DateCF3'=>$_dateCF3,'DateCD4'=>$_dateCD4,'DateCF4'=>$_dateCF4,'DateLO1'=>$_dateLO1,'DateLO2'=>$_dateLO2,'DateLO3'=>$_dateLO3,'DateLO4'=>$_dateLO4),false);
    }
    /**
     * Get DateCD1 value
     * @return string|null
     */
    public function getDateCD1()
    {
        return $this->DateCD1;
    }
    /**
     * Set DateCD1 value
     * @param string $_dateCD1 the DateCD1
     * @return string
     */
    public function setDateCD1($_dateCD1)
    {
        return ($this->DateCD1 = $_dateCD1);
    }
    /**
     * Get DateCF1 value
     * @return string|null
     */
    public function getDateCF1()
    {
        return $this->DateCF1;
    }
    /**
     * Set DateCF1 value
     * @param string $_dateCF1 the DateCF1
     * @return string
     */
    public function setDateCF1($_dateCF1)
    {
        return ($this->DateCF1 = $_dateCF1);
    }
    /**
     * Get DateCD2 value
     * @return string|null
     */
    public function getDateCD2()
    {
        return $this->DateCD2;
    }
    /**
     * Set DateCD2 value
     * @param string $_dateCD2 the DateCD2
     * @return string
     */
    public function setDateCD2($_dateCD2)
    {
        return ($this->DateCD2 = $_dateCD2);
    }
    /**
     * Get DateCF2 value
     * @return string|null
     */
    public function getDateCF2()
    {
        return $this->DateCF2;
    }
    /**
     * Set DateCF2 value
     * @param string $_dateCF2 the DateCF2
     * @return string
     */
    public function setDateCF2($_dateCF2)
    {
        return ($this->DateCF2 = $_dateCF2);
    }
    /**
     * Get DateCD3 value
     * @return string|null
     */
    public function getDateCD3()
    {
        return $this->DateCD3;
    }
    /**
     * Set DateCD3 value
     * @param string $_dateCD3 the DateCD3
     * @return string
     */
    public function setDateCD3($_dateCD3)
    {
        return ($this->DateCD3 = $_dateCD3);
    }
    /**
     * Get DateCF3 value
     * @return string|null
     */
    public function getDateCF3()
    {
        return $this->DateCF3;
    }
    /**
     * Set DateCF3 value
     * @param string $_dateCF3 the DateCF3
     * @return string
     */
    public function setDateCF3($_dateCF3)
    {
        return ($this->DateCF3 = $_dateCF3);
    }
    /**
     * Get DateCD4 value
     * @return string|null
     */
    public function getDateCD4()
    {
        return $this->DateCD4;
    }
    /**
     * Set DateCD4 value
     * @param string $_dateCD4 the DateCD4
     * @return string
     */
    public function setDateCD4($_dateCD4)
    {
        return ($this->DateCD4 = $_dateCD4);
    }
    /**
     * Get DateCF4 value
     * @return string|null
     */
    public function getDateCF4()
    {
        return $this->DateCF4;
    }
    /**
     * Set DateCF4 value
     * @param string $_dateCF4 the DateCF4
     * @return string
     */
    public function setDateCF4($_dateCF4)
    {
        return ($this->DateCF4 = $_dateCF4);
    }
    /**
     * Get DateLO1 value
     * @return string|null
     */
    public function getDateLO1()
    {
        return $this->DateLO1;
    }
    /**
     * Set DateLO1 value
     * @param string $_dateLO1 the DateLO1
     * @return string
     */
    public function setDateLO1($_dateLO1)
    {
        return ($this->DateLO1 = $_dateLO1);
    }
    /**
     * Get DateLO2 value
     * @return string|null
     */
    public function getDateLO2()
    {
        return $this->DateLO2;
    }
    /**
     * Set DateLO2 value
     * @param string $_dateLO2 the DateLO2
     * @return string
     */
    public function setDateLO2($_dateLO2)
    {
        return ($this->DateLO2 = $_dateLO2);
    }
    /**
     * Get DateLO3 value
     * @return string|null
     */
    public function getDateLO3()
    {
        return $this->DateLO3;
    }
    /**
     * Set DateLO3 value
     * @param string $_dateLO3 the DateLO3
     * @return string
     */
    public function setDateLO3($_dateLO3)
    {
        return ($this->DateLO3 = $_dateLO3);
    }
    /**
     * Get DateLO4 value
     * @return string|null
     */
    public function getDateLO4()
    {
        return $this->DateLO4;
    }
    /**
     * Set DateLO4 value
     * @param string $_dateLO4 the DateLO4
     * @return string
     */
    public function setDateLO4($_dateLO4)
    {
        return ($this->DateLO4 = $_dateLO4);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructHolidays
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
