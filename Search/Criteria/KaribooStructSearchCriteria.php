<?php
/**
 * File for class KaribooStructSearchCriteria
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructSearchCriteria originally named SearchCriteria
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructSearchCriteria extends KaribooWsdlClass
{
    /**
     * The Barcode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $Barcode;
    /**
     * The OrderID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $OrderID;
    /**
     * Constructor method for SearchCriteria
     * @see parent::__construct()
     * @param string $_barcode
     * @param string $_orderID
     * @return KaribooStructSearchCriteria
     */
    public function __construct($_barcode = NULL,$_orderID = NULL)
    {
        parent::__construct(array('Barcode'=>$_barcode,'OrderID'=>$_orderID),false);
    }
    /**
     * Get Barcode value
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->Barcode;
    }
    /**
     * Set Barcode value
     * @param string $_barcode the Barcode
     * @return string
     */
    public function setBarcode($_barcode)
    {
        return ($this->Barcode = $_barcode);
    }
    /**
     * Get OrderID value
     * @return string|null
     */
    public function getOrderID()
    {
        return $this->OrderID;
    }
    /**
     * Set OrderID value
     * @param string $_orderID the OrderID
     * @return string
     */
    public function setOrderID($_orderID)
    {
        return ($this->OrderID = $_orderID);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructSearchCriteria
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
