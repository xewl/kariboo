<?php
/**
 * File for class KaribooStructHistoryStatus
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructHistoryStatus originally named HistoryStatus
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructHistoryStatus extends KaribooWsdlClass
{
    /**
     * The LocationID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $LocationID;
    /**
     * The StatusCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $StatusCode;
    /**
     * The StatusDescriptions
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructStatusDescriptions
     */
    public $StatusDescriptions;
    /**
     * The StatusDate
     * @var dateTime
     */
    public $StatusDate;
    /**
     * Constructor method for HistoryStatus
     * @see parent::__construct()
     * @param string $_locationID
     * @param string $_statusCode
     * @param KaribooStructStatusDescriptions $_statusDescriptions
     * @param dateTime $_statusDate
     * @return KaribooStructHistoryStatus
     */
    public function __construct($_locationID = NULL,$_statusCode = NULL,$_statusDescriptions = NULL,$_statusDate = NULL)
    {
        parent::__construct(array('LocationID'=>$_locationID,'StatusCode'=>$_statusCode,'StatusDescriptions'=>$_statusDescriptions,'StatusDate'=>$_statusDate),false);
    }
    /**
     * Get LocationID value
     * @return string|null
     */
    public function getLocationID()
    {
        return $this->LocationID;
    }
    /**
     * Set LocationID value
     * @param string $_locationID the LocationID
     * @return string
     */
    public function setLocationID($_locationID)
    {
        return ($this->LocationID = $_locationID);
    }
    /**
     * Get StatusCode value
     * @return string|null
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }
    /**
     * Set StatusCode value
     * @param string $_statusCode the StatusCode
     * @return string
     */
    public function setStatusCode($_statusCode)
    {
        return ($this->StatusCode = $_statusCode);
    }
    /**
     * Get StatusDescriptions value
     * @return KaribooStructStatusDescriptions|null
     */
    public function getStatusDescriptions()
    {
        return $this->StatusDescriptions;
    }
    /**
     * Set StatusDescriptions value
     * @param KaribooStructStatusDescriptions $_statusDescriptions the StatusDescriptions
     * @return KaribooStructStatusDescriptions
     */
    public function setStatusDescriptions($_statusDescriptions)
    {
        return ($this->StatusDescriptions = $_statusDescriptions);
    }
    /**
     * Get StatusDate value
     * @return dateTime|null
     */
    public function getStatusDate()
    {
        return $this->StatusDate;
    }
    /**
     * Set StatusDate value
     * @param dateTime $_statusDate the StatusDate
     * @return dateTime
     */
    public function setStatusDate($_statusDate)
    {
        return ($this->StatusDate = $_statusDate);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructHistoryStatus
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
