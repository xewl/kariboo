<?php
/**
 * File for class KaribooServiceFinancial
 * @package Kariboo
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooServiceFinancial originally named Financial
 * @package Kariboo
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooServiceFinancial extends KaribooWsdlClass
{
    /**
     * Sets the Authentification SoapHeader param
     * @uses KaribooWsdlClass::setSoapHeader()
     * @param KaribooStructAuthentification $_karibooStructAuthentification
     * @param string $_nameSpace http://www.kariboo.be/ws/services
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderAuthentification(KaribooStructAuthentification $_karibooStructAuthentification,$_nameSpace = 'http://www.kariboo.be/ws/services',$_mustUnderstand = false,$_actor = null)
    {
        return $this->setSoapHeader($_nameSpace,'Authentification',$_karibooStructAuthentification,$_mustUnderstand,$_actor);
    }
    /**
     * Method to call the operation originally named FinancialParcel
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : Authentification
     * - SOAPHeaderNamespaces : http://www.kariboo.be/ws/services
     * - SOAPHeaderTypes : {@link KaribooStructAuthentification}
     * - SOAPHeaders : required
     * @uses KaribooWsdlClass::getSoapClient()
     * @uses KaribooWsdlClass::setResult()
     * @uses KaribooWsdlClass::saveLastError()
     * @param KaribooStructFinancialParcelRequest $_karibooStructFinancialParcelRequest
     * @return KaribooStructFinancialParcelResponse
     */
    public function FinancialParcel(KaribooStructFinancialParcelRequest $_karibooStructFinancialParcelRequest)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->FinancialParcel($_karibooStructFinancialParcelRequest));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see KaribooWsdlClass::getResult()
     * @return KaribooStructFinancialParcelResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
