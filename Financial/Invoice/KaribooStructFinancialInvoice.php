<?php
/**
 * File for class KaribooStructFinancialInvoice
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructFinancialInvoice originally named FinancialInvoice
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructFinancialInvoice extends KaribooWsdlClass
{
    /**
     * The PriceVatExcluded
     * @var float
     */
    public $PriceVatExcluded;
    /**
     * The PromoCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $PromoCode;
    /**
     * The PromoDiscountVatExcluded
     * @var float
     */
    public $PromoDiscountVatExcluded;
    /**
     * The VatAmount
     * @var float
     */
    public $VatAmount;
    /**
     * The VatRate
     * @var float
     */
    public $VatRate;
    /**
     * The InsurancePriceVatExcluded
     * @var float
     */
    public $InsurancePriceVatExcluded;
    /**
     * The InsuranceVatAmount
     * @var float
     */
    public $InsuranceVatAmount;
    /**
     * The InsuranceVatRate
     * @var float
     */
    public $InsuranceVatRate;
    /**
     * Constructor method for FinancialInvoice
     * @see parent::__construct()
     * @param float $_priceVatExcluded
     * @param string $_promoCode
     * @param float $_promoDiscountVatExcluded
     * @param float $_vatAmount
     * @param float $_vatRate
     * @param float $_insurancePriceVatExcluded
     * @param float $_insuranceVatAmount
     * @param float $_insuranceVatRate
     * @return KaribooStructFinancialInvoice
     */
    public function __construct($_priceVatExcluded = NULL,$_promoCode = NULL,$_promoDiscountVatExcluded = NULL,$_vatAmount = NULL,$_vatRate = NULL,$_insurancePriceVatExcluded = NULL,$_insuranceVatAmount = NULL,$_insuranceVatRate = NULL)
    {
        parent::__construct(array('PriceVatExcluded'=>$_priceVatExcluded,'PromoCode'=>$_promoCode,'PromoDiscountVatExcluded'=>$_promoDiscountVatExcluded,'VatAmount'=>$_vatAmount,'VatRate'=>$_vatRate,'InsurancePriceVatExcluded'=>$_insurancePriceVatExcluded,'InsuranceVatAmount'=>$_insuranceVatAmount,'InsuranceVatRate'=>$_insuranceVatRate),false);
    }
    /**
     * Get PriceVatExcluded value
     * @return float|null
     */
    public function getPriceVatExcluded()
    {
        return $this->PriceVatExcluded;
    }
    /**
     * Set PriceVatExcluded value
     * @param float $_priceVatExcluded the PriceVatExcluded
     * @return float
     */
    public function setPriceVatExcluded($_priceVatExcluded)
    {
        return ($this->PriceVatExcluded = $_priceVatExcluded);
    }
    /**
     * Get PromoCode value
     * @return string|null
     */
    public function getPromoCode()
    {
        return $this->PromoCode;
    }
    /**
     * Set PromoCode value
     * @param string $_promoCode the PromoCode
     * @return string
     */
    public function setPromoCode($_promoCode)
    {
        return ($this->PromoCode = $_promoCode);
    }
    /**
     * Get PromoDiscountVatExcluded value
     * @return float|null
     */
    public function getPromoDiscountVatExcluded()
    {
        return $this->PromoDiscountVatExcluded;
    }
    /**
     * Set PromoDiscountVatExcluded value
     * @param float $_promoDiscountVatExcluded the PromoDiscountVatExcluded
     * @return float
     */
    public function setPromoDiscountVatExcluded($_promoDiscountVatExcluded)
    {
        return ($this->PromoDiscountVatExcluded = $_promoDiscountVatExcluded);
    }
    /**
     * Get VatAmount value
     * @return float|null
     */
    public function getVatAmount()
    {
        return $this->VatAmount;
    }
    /**
     * Set VatAmount value
     * @param float $_vatAmount the VatAmount
     * @return float
     */
    public function setVatAmount($_vatAmount)
    {
        return ($this->VatAmount = $_vatAmount);
    }
    /**
     * Get VatRate value
     * @return float|null
     */
    public function getVatRate()
    {
        return $this->VatRate;
    }
    /**
     * Set VatRate value
     * @param float $_vatRate the VatRate
     * @return float
     */
    public function setVatRate($_vatRate)
    {
        return ($this->VatRate = $_vatRate);
    }
    /**
     * Get InsurancePriceVatExcluded value
     * @return float|null
     */
    public function getInsurancePriceVatExcluded()
    {
        return $this->InsurancePriceVatExcluded;
    }
    /**
     * Set InsurancePriceVatExcluded value
     * @param float $_insurancePriceVatExcluded the InsurancePriceVatExcluded
     * @return float
     */
    public function setInsurancePriceVatExcluded($_insurancePriceVatExcluded)
    {
        return ($this->InsurancePriceVatExcluded = $_insurancePriceVatExcluded);
    }
    /**
     * Get InsuranceVatAmount value
     * @return float|null
     */
    public function getInsuranceVatAmount()
    {
        return $this->InsuranceVatAmount;
    }
    /**
     * Set InsuranceVatAmount value
     * @param float $_insuranceVatAmount the InsuranceVatAmount
     * @return float
     */
    public function setInsuranceVatAmount($_insuranceVatAmount)
    {
        return ($this->InsuranceVatAmount = $_insuranceVatAmount);
    }
    /**
     * Get InsuranceVatRate value
     * @return float|null
     */
    public function getInsuranceVatRate()
    {
        return $this->InsuranceVatRate;
    }
    /**
     * Set InsuranceVatRate value
     * @param float $_insuranceVatRate the InsuranceVatRate
     * @return float
     */
    public function setInsuranceVatRate($_insuranceVatRate)
    {
        return ($this->InsuranceVatRate = $_insuranceVatRate);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructFinancialInvoice
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
