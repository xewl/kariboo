<?php
/**
 * File for class KaribooStructFinancialInvoiceDetail
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructFinancialInvoiceDetail originally named FinancialInvoiceDetail
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructFinancialInvoiceDetail extends KaribooWsdlClass
{
    /**
     * The DetailsLine
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructFinancialInvoice
     */
    public $DetailsLine;
    /**
     * The barcode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $barcode;
    /**
     * Constructor method for FinancialInvoiceDetail
     * @see parent::__construct()
     * @param KaribooStructFinancialInvoice $_detailsLine
     * @param string $_barcode
     * @return KaribooStructFinancialInvoiceDetail
     */
    public function __construct($_detailsLine = NULL,$_barcode = NULL)
    {
        parent::__construct(array('DetailsLine'=>$_detailsLine,'barcode'=>$_barcode),false);
    }
    /**
     * Get DetailsLine value
     * @return KaribooStructFinancialInvoice|null
     */
    public function getDetailsLine()
    {
        return $this->DetailsLine;
    }
    /**
     * Set DetailsLine value
     * @param KaribooStructFinancialInvoice $_detailsLine the DetailsLine
     * @return KaribooStructFinancialInvoice
     */
    public function setDetailsLine($_detailsLine)
    {
        return ($this->DetailsLine = $_detailsLine);
    }
    /**
     * Get barcode value
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }
    /**
     * Set barcode value
     * @param string $_barcode the barcode
     * @return string
     */
    public function setBarcode($_barcode)
    {
        return ($this->barcode = $_barcode);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructFinancialInvoiceDetail
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
