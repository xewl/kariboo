<?php
/**
 * File for class KaribooStructFinancialParcelRequest
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructFinancialParcelRequest originally named FinancialParcelRequest
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd0}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructFinancialParcelRequest extends KaribooWsdlClass
{
    /**
     * The TransactionId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $TransactionId;
    /**
     * The OrderId
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $OrderId;
    /**
     * The TotalInvoice
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructFinancialInvoice
     */
    public $TotalInvoice;
    /**
     * The DetailsInvoice
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructArrayOfInvoiceDetails
     */
    public $DetailsInvoice;
    /**
     * The PaymentMethod
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $PaymentMethod;
    /**
     * The GetInvoice
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumYesNo
     */
    public $GetInvoice;
    /**
     * The AddressInvoice
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var KaribooStructAnnounceAddress
     */
    public $AddressInvoice;
    /**
     * The Language
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumUserLanguage
     */
    public $Language;
    /**
     * Constructor method for FinancialParcelRequest
     * @see parent::__construct()
     * @param string $_transactionId
     * @param string $_orderId
     * @param KaribooStructFinancialInvoice $_totalInvoice
     * @param KaribooStructArrayOfInvoiceDetails $_detailsInvoice
     * @param string $_paymentMethod
     * @param KaribooEnumYesNo $_getInvoice
     * @param KaribooStructAnnounceAddress $_addressInvoice
     * @param KaribooEnumUserLanguage $_language
     * @return KaribooStructFinancialParcelRequest
     */
    public function __construct($_transactionId = NULL,$_orderId = NULL,$_totalInvoice = NULL,$_detailsInvoice = NULL,$_paymentMethod = NULL,$_getInvoice = NULL,$_addressInvoice = NULL,$_language = NULL)
    {
        parent::__construct(array('TransactionId'=>$_transactionId,'OrderId'=>$_orderId,'TotalInvoice'=>$_totalInvoice,'DetailsInvoice'=>($_detailsInvoice instanceof KaribooStructArrayOfInvoiceDetails)?$_detailsInvoice:new KaribooStructArrayOfInvoiceDetails($_detailsInvoice),'PaymentMethod'=>$_paymentMethod,'GetInvoice'=>$_getInvoice,'AddressInvoice'=>$_addressInvoice,'Language'=>$_language),false);
    }
    /**
     * Get TransactionId value
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->TransactionId;
    }
    /**
     * Set TransactionId value
     * @param string $_transactionId the TransactionId
     * @return string
     */
    public function setTransactionId($_transactionId)
    {
        return ($this->TransactionId = $_transactionId);
    }
    /**
     * Get OrderId value
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->OrderId;
    }
    /**
     * Set OrderId value
     * @param string $_orderId the OrderId
     * @return string
     */
    public function setOrderId($_orderId)
    {
        return ($this->OrderId = $_orderId);
    }
    /**
     * Get TotalInvoice value
     * @return KaribooStructFinancialInvoice|null
     */
    public function getTotalInvoice()
    {
        return $this->TotalInvoice;
    }
    /**
     * Set TotalInvoice value
     * @param KaribooStructFinancialInvoice $_totalInvoice the TotalInvoice
     * @return KaribooStructFinancialInvoice
     */
    public function setTotalInvoice($_totalInvoice)
    {
        return ($this->TotalInvoice = $_totalInvoice);
    }
    /**
     * Get DetailsInvoice value
     * @return KaribooStructArrayOfInvoiceDetails|null
     */
    public function getDetailsInvoice()
    {
        return $this->DetailsInvoice;
    }
    /**
     * Set DetailsInvoice value
     * @param KaribooStructArrayOfInvoiceDetails $_detailsInvoice the DetailsInvoice
     * @return KaribooStructArrayOfInvoiceDetails
     */
    public function setDetailsInvoice($_detailsInvoice)
    {
        return ($this->DetailsInvoice = $_detailsInvoice);
    }
    /**
     * Get PaymentMethod value
     * @return string|null
     */
    public function getPaymentMethod()
    {
        return $this->PaymentMethod;
    }
    /**
     * Set PaymentMethod value
     * @param string $_paymentMethod the PaymentMethod
     * @return string
     */
    public function setPaymentMethod($_paymentMethod)
    {
        return ($this->PaymentMethod = $_paymentMethod);
    }
    /**
     * Get GetInvoice value
     * @return KaribooEnumYesNo|null
     */
    public function getGetInvoice()
    {
        return $this->GetInvoice;
    }
    /**
     * Set GetInvoice value
     * @uses KaribooEnumYesNo::valueIsValid()
     * @param KaribooEnumYesNo $_getInvoice the GetInvoice
     * @return KaribooEnumYesNo
     */
    public function setGetInvoice($_getInvoice)
    {
        if(!KaribooEnumYesNo::valueIsValid($_getInvoice))
        {
            return false;
        }
        return ($this->GetInvoice = $_getInvoice);
    }
    /**
     * Get AddressInvoice value
     * @return KaribooStructAnnounceAddress|null
     */
    public function getAddressInvoice()
    {
        return $this->AddressInvoice;
    }
    /**
     * Set AddressInvoice value
     * @param KaribooStructAnnounceAddress $_addressInvoice the AddressInvoice
     * @return KaribooStructAnnounceAddress
     */
    public function setAddressInvoice($_addressInvoice)
    {
        return ($this->AddressInvoice = $_addressInvoice);
    }
    /**
     * Get Language value
     * @return KaribooEnumUserLanguage|null
     */
    public function getLanguage()
    {
        return $this->Language;
    }
    /**
     * Set Language value
     * @uses KaribooEnumUserLanguage::valueIsValid()
     * @param KaribooEnumUserLanguage $_language the Language
     * @return KaribooEnumUserLanguage
     */
    public function setLanguage($_language)
    {
        if(!KaribooEnumUserLanguage::valueIsValid($_language))
        {
            return false;
        }
        return ($this->Language = $_language);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructFinancialParcelRequest
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
