<?php
/**
 * File for class KaribooStructShipment
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructShipment originally named Shipment
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd2}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructShipment extends KaribooWsdlClass
{
    /**
     * The PackageSizeX
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $PackageSizeX;
    /**
     * The PackageSizeY
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $PackageSizeY;
    /**
     * The PackageSizeZ
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $PackageSizeZ;
    /**
     * The PackageType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var KaribooEnumPackageType
     */
    public $PackageType;
    /**
     * The PackageValue
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var float
     */
    public $PackageValue;
    /**
     * The PackageVolume
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $PackageVolume;
    /**
     * The PackageWeight
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $PackageWeight;
    /**
     * Constructor method for Shipment
     * @see parent::__construct()
     * @param int $_packageSizeX
     * @param int $_packageSizeY
     * @param int $_packageSizeZ
     * @param KaribooEnumPackageType $_packageType
     * @param float $_packageValue
     * @param int $_packageVolume
     * @param int $_packageWeight
     * @return KaribooStructShipment
     */
    public function __construct($_packageSizeX = NULL,$_packageSizeY = NULL,$_packageSizeZ = NULL,$_packageType = NULL,$_packageValue = NULL,$_packageVolume = NULL,$_packageWeight = NULL)
    {
        parent::__construct(array('PackageSizeX'=>$_packageSizeX,'PackageSizeY'=>$_packageSizeY,'PackageSizeZ'=>$_packageSizeZ,'PackageType'=>$_packageType,'PackageValue'=>$_packageValue,'PackageVolume'=>$_packageVolume,'PackageWeight'=>$_packageWeight),false);
    }
    /**
     * Get PackageSizeX value
     * @return int|null
     */
    public function getPackageSizeX()
    {
        return $this->PackageSizeX;
    }
    /**
     * Set PackageSizeX value
     * @param int $_packageSizeX the PackageSizeX
     * @return int
     */
    public function setPackageSizeX($_packageSizeX)
    {
        return ($this->PackageSizeX = $_packageSizeX);
    }
    /**
     * Get PackageSizeY value
     * @return int|null
     */
    public function getPackageSizeY()
    {
        return $this->PackageSizeY;
    }
    /**
     * Set PackageSizeY value
     * @param int $_packageSizeY the PackageSizeY
     * @return int
     */
    public function setPackageSizeY($_packageSizeY)
    {
        return ($this->PackageSizeY = $_packageSizeY);
    }
    /**
     * Get PackageSizeZ value
     * @return int|null
     */
    public function getPackageSizeZ()
    {
        return $this->PackageSizeZ;
    }
    /**
     * Set PackageSizeZ value
     * @param int $_packageSizeZ the PackageSizeZ
     * @return int
     */
    public function setPackageSizeZ($_packageSizeZ)
    {
        return ($this->PackageSizeZ = $_packageSizeZ);
    }
    /**
     * Get PackageType value
     * @return KaribooEnumPackageType|null
     */
    public function getPackageType()
    {
        return $this->PackageType;
    }
    /**
     * Set PackageType value
     * @uses KaribooEnumPackageType::valueIsValid()
     * @param KaribooEnumPackageType $_packageType the PackageType
     * @return KaribooEnumPackageType
     */
    public function setPackageType($_packageType)
    {
        if(!KaribooEnumPackageType::valueIsValid($_packageType))
        {
            return false;
        }
        return ($this->PackageType = $_packageType);
    }
    /**
     * Get PackageValue value
     * @return float|null
     */
    public function getPackageValue()
    {
        return $this->PackageValue;
    }
    /**
     * Set PackageValue value
     * @param float $_packageValue the PackageValue
     * @return float
     */
    public function setPackageValue($_packageValue)
    {
        return ($this->PackageValue = $_packageValue);
    }
    /**
     * Get PackageVolume value
     * @return int|null
     */
    public function getPackageVolume()
    {
        return $this->PackageVolume;
    }
    /**
     * Set PackageVolume value
     * @param int $_packageVolume the PackageVolume
     * @return int
     */
    public function setPackageVolume($_packageVolume)
    {
        return ($this->PackageVolume = $_packageVolume);
    }
    /**
     * Get PackageWeight value
     * @return int|null
     */
    public function getPackageWeight()
    {
        return $this->PackageWeight;
    }
    /**
     * Set PackageWeight value
     * @param int $_packageWeight the PackageWeight
     * @return int
     */
    public function setPackageWeight($_packageWeight)
    {
        return ($this->PackageWeight = $_packageWeight);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructShipment
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
