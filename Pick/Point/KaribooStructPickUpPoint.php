<?php
/**
 * File for class KaribooStructPickUpPoint
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructPickUpPoint originally named PickUpPoint
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructPickUpPoint extends KaribooWsdlClass
{
    /**
     * The ShopID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopID;
    /**
     * The ShopName
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopName;
    /**
     * The ShopStreet
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopStreet;
    /**
     * The ShopStreetNumber
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopStreetNumber;
    /**
     * The ShopBox
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopBox;
    /**
     * The ShopPostalCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopPostalCode;
    /**
     * The ShopCity
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopCity;
    /**
     * The ShopCountry
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopCountry;
    /**
     * The ShopAddressDescription
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopAddressDescription;
    /**
     * The ShopEmail
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopEmail;
    /**
     * The ShopLogisticID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopLogisticID;
    /**
     * The ShopRouteID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopRouteID;
    /**
     * The ShopWarehouseID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopWarehouseID;
    /**
     * The SortID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $SortID;
    /**
     * The ShopStartDate
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopStartDate;
    /**
     * The ShopEndDate
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopEndDate;
    /**
     * The ShopLatitude
     * @var float
     */
    public $ShopLatitude;
    /**
     * The ShopLongitude
     * @var float
     */
    public $ShopLongitude;
    /**
     * The WinkelID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $WinkelID;
    /**
     * The ShopHolidays
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructHolidays
     */
    public $ShopHolidays;
    /**
     * The ShopOpeningHours
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var KaribooStructOpeningHours
     */
    public $ShopOpeningHours;
    /**
     * The COD
     * @var boolean
     */
    public $COD;
    /**
     * The Distance
     * @var int
     */
    public $Distance;
    /**
     * The ShopPhoto
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopPhoto;
    /**
     * The ModifyDate
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ModifyDate;
    /**
     * The Active
     * @var boolean
     */
    public $Active;
    /**
     * The DateLastParcel
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $DateLastParcel;
    /**
     * The DateClosure
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $DateClosure;
    /**
     * The DateFirstParcel
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $DateFirstParcel;
    /**
     * The OpenOnSunday
     * @var boolean
     */
    public $OpenOnSunday;
    /**
     * The OpenAfter1600
     * @var boolean
     */
    public $OpenAfter1600;
    /**
     * The LOURD10
     * @var boolean
     */
    public $LOURD10;
    /**
     * The LOURD20
     * @var boolean
     */
    public $LOURD20;
    /**
     * The LOURD30
     * @var boolean
     */
    public $LOURD30;
    /**
     * The LOURD99
     * @var boolean
     */
    public $LOURD99;
    /**
     * The ShopSubstitution
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShopSubstitution;
    /**
     * Constructor method for PickUpPoint
     * @see parent::__construct()
     * @param string $_shopID
     * @param string $_shopName
     * @param string $_shopStreet
     * @param string $_shopStreetNumber
     * @param string $_shopBox
     * @param string $_shopPostalCode
     * @param string $_shopCity
     * @param string $_shopCountry
     * @param string $_shopAddressDescription
     * @param string $_shopEmail
     * @param string $_shopLogisticID
     * @param string $_shopRouteID
     * @param string $_shopWarehouseID
     * @param string $_sortID
     * @param string $_shopStartDate
     * @param string $_shopEndDate
     * @param float $_shopLatitude
     * @param float $_shopLongitude
     * @param string $_winkelID
     * @param KaribooStructHolidays $_shopHolidays
     * @param KaribooStructOpeningHours $_shopOpeningHours
     * @param boolean $_cOD
     * @param int $_distance
     * @param string $_shopPhoto
     * @param string $_modifyDate
     * @param boolean $_active
     * @param string $_dateLastParcel
     * @param string $_dateClosure
     * @param string $_dateFirstParcel
     * @param boolean $_openOnSunday
     * @param boolean $_openAfter1600
     * @param boolean $_lOURD10
     * @param boolean $_lOURD20
     * @param boolean $_lOURD30
     * @param boolean $_lOURD99
     * @param string $_shopSubstitution
     * @return KaribooStructPickUpPoint
     */
    public function __construct($_shopID = NULL,$_shopName = NULL,$_shopStreet = NULL,$_shopStreetNumber = NULL,$_shopBox = NULL,$_shopPostalCode = NULL,$_shopCity = NULL,$_shopCountry = NULL,$_shopAddressDescription = NULL,$_shopEmail = NULL,$_shopLogisticID = NULL,$_shopRouteID = NULL,$_shopWarehouseID = NULL,$_sortID = NULL,$_shopStartDate = NULL,$_shopEndDate = NULL,$_shopLatitude = NULL,$_shopLongitude = NULL,$_winkelID = NULL,$_shopHolidays = NULL,$_shopOpeningHours = NULL,$_cOD = NULL,$_distance = NULL,$_shopPhoto = NULL,$_modifyDate = NULL,$_active = NULL,$_dateLastParcel = NULL,$_dateClosure = NULL,$_dateFirstParcel = NULL,$_openOnSunday = NULL,$_openAfter1600 = NULL,$_lOURD10 = NULL,$_lOURD20 = NULL,$_lOURD30 = NULL,$_lOURD99 = NULL,$_shopSubstitution = NULL)
    {
        parent::__construct(array('ShopID'=>$_shopID,'ShopName'=>$_shopName,'ShopStreet'=>$_shopStreet,'ShopStreetNumber'=>$_shopStreetNumber,'ShopBox'=>$_shopBox,'ShopPostalCode'=>$_shopPostalCode,'ShopCity'=>$_shopCity,'ShopCountry'=>$_shopCountry,'ShopAddressDescription'=>$_shopAddressDescription,'ShopEmail'=>$_shopEmail,'ShopLogisticID'=>$_shopLogisticID,'ShopRouteID'=>$_shopRouteID,'ShopWarehouseID'=>$_shopWarehouseID,'SortID'=>$_sortID,'ShopStartDate'=>$_shopStartDate,'ShopEndDate'=>$_shopEndDate,'ShopLatitude'=>$_shopLatitude,'ShopLongitude'=>$_shopLongitude,'WinkelID'=>$_winkelID,'ShopHolidays'=>$_shopHolidays,'ShopOpeningHours'=>$_shopOpeningHours,'COD'=>$_cOD,'Distance'=>$_distance,'ShopPhoto'=>$_shopPhoto,'ModifyDate'=>$_modifyDate,'Active'=>$_active,'DateLastParcel'=>$_dateLastParcel,'DateClosure'=>$_dateClosure,'DateFirstParcel'=>$_dateFirstParcel,'OpenOnSunday'=>$_openOnSunday,'OpenAfter1600'=>$_openAfter1600,'LOURD10'=>$_lOURD10,'LOURD20'=>$_lOURD20,'LOURD30'=>$_lOURD30,'LOURD99'=>$_lOURD99,'ShopSubstitution'=>$_shopSubstitution),false);
    }
    /**
     * Get ShopID value
     * @return string|null
     */
    public function getShopID()
    {
        return $this->ShopID;
    }
    /**
     * Set ShopID value
     * @param string $_shopID the ShopID
     * @return string
     */
    public function setShopID($_shopID)
    {
        return ($this->ShopID = $_shopID);
    }
    /**
     * Get ShopName value
     * @return string|null
     */
    public function getShopName()
    {
        return $this->ShopName;
    }
    /**
     * Set ShopName value
     * @param string $_shopName the ShopName
     * @return string
     */
    public function setShopName($_shopName)
    {
        return ($this->ShopName = $_shopName);
    }
    /**
     * Get ShopStreet value
     * @return string|null
     */
    public function getShopStreet()
    {
        return $this->ShopStreet;
    }
    /**
     * Set ShopStreet value
     * @param string $_shopStreet the ShopStreet
     * @return string
     */
    public function setShopStreet($_shopStreet)
    {
        return ($this->ShopStreet = $_shopStreet);
    }
    /**
     * Get ShopStreetNumber value
     * @return string|null
     */
    public function getShopStreetNumber()
    {
        return $this->ShopStreetNumber;
    }
    /**
     * Set ShopStreetNumber value
     * @param string $_shopStreetNumber the ShopStreetNumber
     * @return string
     */
    public function setShopStreetNumber($_shopStreetNumber)
    {
        return ($this->ShopStreetNumber = $_shopStreetNumber);
    }
    /**
     * Get ShopBox value
     * @return string|null
     */
    public function getShopBox()
    {
        return $this->ShopBox;
    }
    /**
     * Set ShopBox value
     * @param string $_shopBox the ShopBox
     * @return string
     */
    public function setShopBox($_shopBox)
    {
        return ($this->ShopBox = $_shopBox);
    }
    /**
     * Get ShopPostalCode value
     * @return string|null
     */
    public function getShopPostalCode()
    {
        return $this->ShopPostalCode;
    }
    /**
     * Set ShopPostalCode value
     * @param string $_shopPostalCode the ShopPostalCode
     * @return string
     */
    public function setShopPostalCode($_shopPostalCode)
    {
        return ($this->ShopPostalCode = $_shopPostalCode);
    }
    /**
     * Get ShopCity value
     * @return string|null
     */
    public function getShopCity()
    {
        return $this->ShopCity;
    }
    /**
     * Set ShopCity value
     * @param string $_shopCity the ShopCity
     * @return string
     */
    public function setShopCity($_shopCity)
    {
        return ($this->ShopCity = $_shopCity);
    }
    /**
     * Get ShopCountry value
     * @return string|null
     */
    public function getShopCountry()
    {
        return $this->ShopCountry;
    }
    /**
     * Set ShopCountry value
     * @param string $_shopCountry the ShopCountry
     * @return string
     */
    public function setShopCountry($_shopCountry)
    {
        return ($this->ShopCountry = $_shopCountry);
    }
    /**
     * Get ShopAddressDescription value
     * @return string|null
     */
    public function getShopAddressDescription()
    {
        return $this->ShopAddressDescription;
    }
    /**
     * Set ShopAddressDescription value
     * @param string $_shopAddressDescription the ShopAddressDescription
     * @return string
     */
    public function setShopAddressDescription($_shopAddressDescription)
    {
        return ($this->ShopAddressDescription = $_shopAddressDescription);
    }
    /**
     * Get ShopEmail value
     * @return string|null
     */
    public function getShopEmail()
    {
        return $this->ShopEmail;
    }
    /**
     * Set ShopEmail value
     * @param string $_shopEmail the ShopEmail
     * @return string
     */
    public function setShopEmail($_shopEmail)
    {
        return ($this->ShopEmail = $_shopEmail);
    }
    /**
     * Get ShopLogisticID value
     * @return string|null
     */
    public function getShopLogisticID()
    {
        return $this->ShopLogisticID;
    }
    /**
     * Set ShopLogisticID value
     * @param string $_shopLogisticID the ShopLogisticID
     * @return string
     */
    public function setShopLogisticID($_shopLogisticID)
    {
        return ($this->ShopLogisticID = $_shopLogisticID);
    }
    /**
     * Get ShopRouteID value
     * @return string|null
     */
    public function getShopRouteID()
    {
        return $this->ShopRouteID;
    }
    /**
     * Set ShopRouteID value
     * @param string $_shopRouteID the ShopRouteID
     * @return string
     */
    public function setShopRouteID($_shopRouteID)
    {
        return ($this->ShopRouteID = $_shopRouteID);
    }
    /**
     * Get ShopWarehouseID value
     * @return string|null
     */
    public function getShopWarehouseID()
    {
        return $this->ShopWarehouseID;
    }
    /**
     * Set ShopWarehouseID value
     * @param string $_shopWarehouseID the ShopWarehouseID
     * @return string
     */
    public function setShopWarehouseID($_shopWarehouseID)
    {
        return ($this->ShopWarehouseID = $_shopWarehouseID);
    }
    /**
     * Get SortID value
     * @return string|null
     */
    public function getSortID()
    {
        return $this->SortID;
    }
    /**
     * Set SortID value
     * @param string $_sortID the SortID
     * @return string
     */
    public function setSortID($_sortID)
    {
        return ($this->SortID = $_sortID);
    }
    /**
     * Get ShopStartDate value
     * @return string|null
     */
    public function getShopStartDate()
    {
        return $this->ShopStartDate;
    }
    /**
     * Set ShopStartDate value
     * @param string $_shopStartDate the ShopStartDate
     * @return string
     */
    public function setShopStartDate($_shopStartDate)
    {
        return ($this->ShopStartDate = $_shopStartDate);
    }
    /**
     * Get ShopEndDate value
     * @return string|null
     */
    public function getShopEndDate()
    {
        return $this->ShopEndDate;
    }
    /**
     * Set ShopEndDate value
     * @param string $_shopEndDate the ShopEndDate
     * @return string
     */
    public function setShopEndDate($_shopEndDate)
    {
        return ($this->ShopEndDate = $_shopEndDate);
    }
    /**
     * Get ShopLatitude value
     * @return float|null
     */
    public function getShopLatitude()
    {
        return $this->ShopLatitude;
    }
    /**
     * Set ShopLatitude value
     * @param float $_shopLatitude the ShopLatitude
     * @return float
     */
    public function setShopLatitude($_shopLatitude)
    {
        return ($this->ShopLatitude = $_shopLatitude);
    }
    /**
     * Get ShopLongitude value
     * @return float|null
     */
    public function getShopLongitude()
    {
        return $this->ShopLongitude;
    }
    /**
     * Set ShopLongitude value
     * @param float $_shopLongitude the ShopLongitude
     * @return float
     */
    public function setShopLongitude($_shopLongitude)
    {
        return ($this->ShopLongitude = $_shopLongitude);
    }
    /**
     * Get WinkelID value
     * @return string|null
     */
    public function getWinkelID()
    {
        return $this->WinkelID;
    }
    /**
     * Set WinkelID value
     * @param string $_winkelID the WinkelID
     * @return string
     */
    public function setWinkelID($_winkelID)
    {
        return ($this->WinkelID = $_winkelID);
    }
    /**
     * Get ShopHolidays value
     * @return KaribooStructHolidays|null
     */
    public function getShopHolidays()
    {
        return $this->ShopHolidays;
    }
    /**
     * Set ShopHolidays value
     * @param KaribooStructHolidays $_shopHolidays the ShopHolidays
     * @return KaribooStructHolidays
     */
    public function setShopHolidays($_shopHolidays)
    {
        return ($this->ShopHolidays = $_shopHolidays);
    }
    /**
     * Get ShopOpeningHours value
     * @return KaribooStructOpeningHours|null
     */
    public function getShopOpeningHours()
    {
        return $this->ShopOpeningHours;
    }
    /**
     * Set ShopOpeningHours value
     * @param KaribooStructOpeningHours $_shopOpeningHours the ShopOpeningHours
     * @return KaribooStructOpeningHours
     */
    public function setShopOpeningHours($_shopOpeningHours)
    {
        return ($this->ShopOpeningHours = $_shopOpeningHours);
    }
    /**
     * Get COD value
     * @return boolean|null
     */
    public function getCOD()
    {
        return $this->COD;
    }
    /**
     * Set COD value
     * @param boolean $_cOD the COD
     * @return boolean
     */
    public function setCOD($_cOD)
    {
        return ($this->COD = $_cOD);
    }
    /**
     * Get Distance value
     * @return int|null
     */
    public function getDistance()
    {
        return $this->Distance;
    }
    /**
     * Set Distance value
     * @param int $_distance the Distance
     * @return int
     */
    public function setDistance($_distance)
    {
        return ($this->Distance = $_distance);
    }
    /**
     * Get ShopPhoto value
     * @return string|null
     */
    public function getShopPhoto()
    {
        return $this->ShopPhoto;
    }
    /**
     * Set ShopPhoto value
     * @param string $_shopPhoto the ShopPhoto
     * @return string
     */
    public function setShopPhoto($_shopPhoto)
    {
        return ($this->ShopPhoto = $_shopPhoto);
    }
    /**
     * Get ModifyDate value
     * @return string|null
     */
    public function getModifyDate()
    {
        return $this->ModifyDate;
    }
    /**
     * Set ModifyDate value
     * @param string $_modifyDate the ModifyDate
     * @return string
     */
    public function setModifyDate($_modifyDate)
    {
        return ($this->ModifyDate = $_modifyDate);
    }
    /**
     * Get Active value
     * @return boolean|null
     */
    public function getActive()
    {
        return $this->Active;
    }
    /**
     * Set Active value
     * @param boolean $_active the Active
     * @return boolean
     */
    public function setActive($_active)
    {
        return ($this->Active = $_active);
    }
    /**
     * Get DateLastParcel value
     * @return string|null
     */
    public function getDateLastParcel()
    {
        return $this->DateLastParcel;
    }
    /**
     * Set DateLastParcel value
     * @param string $_dateLastParcel the DateLastParcel
     * @return string
     */
    public function setDateLastParcel($_dateLastParcel)
    {
        return ($this->DateLastParcel = $_dateLastParcel);
    }
    /**
     * Get DateClosure value
     * @return string|null
     */
    public function getDateClosure()
    {
        return $this->DateClosure;
    }
    /**
     * Set DateClosure value
     * @param string $_dateClosure the DateClosure
     * @return string
     */
    public function setDateClosure($_dateClosure)
    {
        return ($this->DateClosure = $_dateClosure);
    }
    /**
     * Get DateFirstParcel value
     * @return string|null
     */
    public function getDateFirstParcel()
    {
        return $this->DateFirstParcel;
    }
    /**
     * Set DateFirstParcel value
     * @param string $_dateFirstParcel the DateFirstParcel
     * @return string
     */
    public function setDateFirstParcel($_dateFirstParcel)
    {
        return ($this->DateFirstParcel = $_dateFirstParcel);
    }
    /**
     * Get OpenOnSunday value
     * @return boolean|null
     */
    public function getOpenOnSunday()
    {
        return $this->OpenOnSunday;
    }
    /**
     * Set OpenOnSunday value
     * @param boolean $_openOnSunday the OpenOnSunday
     * @return boolean
     */
    public function setOpenOnSunday($_openOnSunday)
    {
        return ($this->OpenOnSunday = $_openOnSunday);
    }
    /**
     * Get OpenAfter1600 value
     * @return boolean|null
     */
    public function getOpenAfter1600()
    {
        return $this->OpenAfter1600;
    }
    /**
     * Set OpenAfter1600 value
     * @param boolean $_openAfter1600 the OpenAfter1600
     * @return boolean
     */
    public function setOpenAfter1600($_openAfter1600)
    {
        return ($this->OpenAfter1600 = $_openAfter1600);
    }
    /**
     * Get LOURD10 value
     * @return boolean|null
     */
    public function getLOURD10()
    {
        return $this->LOURD10;
    }
    /**
     * Set LOURD10 value
     * @param boolean $_lOURD10 the LOURD10
     * @return boolean
     */
    public function setLOURD10($_lOURD10)
    {
        return ($this->LOURD10 = $_lOURD10);
    }
    /**
     * Get LOURD20 value
     * @return boolean|null
     */
    public function getLOURD20()
    {
        return $this->LOURD20;
    }
    /**
     * Set LOURD20 value
     * @param boolean $_lOURD20 the LOURD20
     * @return boolean
     */
    public function setLOURD20($_lOURD20)
    {
        return ($this->LOURD20 = $_lOURD20);
    }
    /**
     * Get LOURD30 value
     * @return boolean|null
     */
    public function getLOURD30()
    {
        return $this->LOURD30;
    }
    /**
     * Set LOURD30 value
     * @param boolean $_lOURD30 the LOURD30
     * @return boolean
     */
    public function setLOURD30($_lOURD30)
    {
        return ($this->LOURD30 = $_lOURD30);
    }
    /**
     * Get LOURD99 value
     * @return boolean|null
     */
    public function getLOURD99()
    {
        return $this->LOURD99;
    }
    /**
     * Set LOURD99 value
     * @param boolean $_lOURD99 the LOURD99
     * @return boolean
     */
    public function setLOURD99($_lOURD99)
    {
        return ($this->LOURD99 = $_lOURD99);
    }
    /**
     * Get ShopSubstitution value
     * @return string|null
     */
    public function getShopSubstitution()
    {
        return $this->ShopSubstitution;
    }
    /**
     * Set ShopSubstitution value
     * @param string $_shopSubstitution the ShopSubstitution
     * @return string
     */
    public function setShopSubstitution($_shopSubstitution)
    {
        return ($this->ShopSubstitution = $_shopSubstitution);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructPickUpPoint
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
