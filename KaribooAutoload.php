<?php
/**
 * File to load generated classes once at once time
 * @package Kariboo
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
require_once dirname(__FILE__) . '/KaribooWsdlClass.php';
require_once dirname(__FILE__) . '/Financial/Detail/KaribooStructFinancialInvoiceDetail.php';
require_once dirname(__FILE__) . '/Authentification/KaribooStructAuthentification.php';
require_once dirname(__FILE__) . '/Errors/KaribooStructErrors.php';
require_once dirname(__FILE__) . '/Spot/Request/KaribooStructSpotAroundMeRequest.php';
require_once dirname(__FILE__) . '/Response/Shipments/KaribooStructResponseShipments.php';
require_once dirname(__FILE__) . '/Array/Shipments/KaribooStructArrayOfResponseShipments.php';
require_once dirname(__FILE__) . '/Product/KaribooStructProduct.php';
require_once dirname(__FILE__) . '/Array/Shipment/KaribooStructArrayOfShipment.php';
require_once dirname(__FILE__) . '/Shipment/KaribooStructShipment.php';
require_once dirname(__FILE__) . '/Package/Type/KaribooEnumPackageType.php';
require_once dirname(__FILE__) . '/Spot/Response/KaribooStructSpotResponse.php';
require_once dirname(__FILE__) . '/Spot/Request/KaribooStructSpotRequest.php';
require_once dirname(__FILE__) . '/Financial/Response/KaribooStructFinancialParcelResponse.php';
require_once dirname(__FILE__) . '/Financial/Request/KaribooStructFinancialParcelRequest.php';
require_once dirname(__FILE__) . '/Announce/Response/KaribooStructAnnounceResponse.php';
require_once dirname(__FILE__) . '/Tracking/Request/KaribooStructTrackingRequest.php';
require_once dirname(__FILE__) . '/Tracking/Response/KaribooStructTrackingResponse.php';
require_once dirname(__FILE__) . '/Announce/Request/KaribooStructAnnounceRequest.php';
require_once dirname(__FILE__) . '/Array/Product/KaribooStructArrayOfProduct.php';
require_once dirname(__FILE__) . '/User/Language/KaribooEnumUserLanguage.php';
require_once dirname(__FILE__) . '/Tracking/KaribooStructTracking.php';
require_once dirname(__FILE__) . '/Tracking/Informations/KaribooStructTrackingContactInformations.php';
require_once dirname(__FILE__) . '/Array/Status/KaribooStructArrayOfHistoryStatus.php';
require_once dirname(__FILE__) . '/History/Status/KaribooStructHistoryStatus.php';
require_once dirname(__FILE__) . '/Array/Tracking/KaribooStructArrayOfTracking.php';
require_once dirname(__FILE__) . '/Search/Criteria/KaribooStructSearchCriteria.php';
require_once dirname(__FILE__) . '/Pick/Point/KaribooStructPickUpPoint.php';
require_once dirname(__FILE__) . '/Holidays/KaribooStructHolidays.php';
require_once dirname(__FILE__) . '/Opening/Hours/KaribooStructOpeningHours.php';
require_once dirname(__FILE__) . '/Array/Criteria/KaribooStructArrayOfSearchCriteria.php';
require_once dirname(__FILE__) . '/Status/Descriptions/KaribooStructStatusDescriptions.php';
require_once dirname(__FILE__) . '/Array/Address/KaribooStructArrayOfAnnounceAddress.php';
require_once dirname(__FILE__) . '/Announce/Parcel/KaribooStructAnnounceParcel.php';
require_once dirname(__FILE__) . '/Label/Configuration/KaribooStructLabelConfiguration.php';
require_once dirname(__FILE__) . '/Label/Format/KaribooEnumLabelChoiceFormat.php';
require_once dirname(__FILE__) . '/Label/Type/KaribooEnumLabelChoiceType.php';
require_once dirname(__FILE__) . '/Operation/Type/KaribooEnumOperationType.php';
require_once dirname(__FILE__) . '/Yes/No/KaribooEnumYesNo.php';
require_once dirname(__FILE__) . '/Announce/Address/KaribooStructAnnounceAddress.php';
require_once dirname(__FILE__) . '/Financial/Invoice/KaribooStructFinancialInvoice.php';
require_once dirname(__FILE__) . '/Array/Details/KaribooStructArrayOfInvoiceDetails.php';
require_once dirname(__FILE__) . '/Array/Point/KaribooStructArrayOfPickUpPoint.php';
require_once dirname(__FILE__) . '/Spot/KaribooServiceSpot.php';
require_once dirname(__FILE__) . '/Client/KaribooServiceClient.php';
require_once dirname(__FILE__) . '/Announce/KaribooServiceAnnounce.php';
require_once dirname(__FILE__) . '/Financial/KaribooServiceFinancial.php';
require_once dirname(__FILE__) . '/KaribooClassMap.php';
