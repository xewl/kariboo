<?php
/**
 * File for the class which returns the class map definition
 * @package Kariboo
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * Class which returns the class map definition by the static method KaribooClassMap::classMap()
 * @package Kariboo
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array (
  'AnnounceAddress' => 'KaribooStructAnnounceAddress',
  'AnnounceParcel' => 'KaribooStructAnnounceParcel',
  'AnnounceRequest' => 'KaribooStructAnnounceRequest',
  'AnnounceResponse' => 'KaribooStructAnnounceResponse',
  'ArrayOfAnnounceAddress' => 'KaribooStructArrayOfAnnounceAddress',
  'ArrayOfHistoryStatus' => 'KaribooStructArrayOfHistoryStatus',
  'ArrayOfInvoiceDetails' => 'KaribooStructArrayOfInvoiceDetails',
  'ArrayOfPickUpPoint' => 'KaribooStructArrayOfPickUpPoint',
  'ArrayOfProduct' => 'KaribooStructArrayOfProduct',
  'ArrayOfResponseShipments' => 'KaribooStructArrayOfResponseShipments',
  'ArrayOfSearchCriteria' => 'KaribooStructArrayOfSearchCriteria',
  'ArrayOfShipment' => 'KaribooStructArrayOfShipment',
  'ArrayOfTracking' => 'KaribooStructArrayOfTracking',
  'Authentification' => 'KaribooStructAuthentification',
  'Errors' => 'KaribooStructErrors',
  'FinancialInvoice' => 'KaribooStructFinancialInvoice',
  'FinancialInvoiceDetail' => 'KaribooStructFinancialInvoiceDetail',
  'FinancialParcelRequest' => 'KaribooStructFinancialParcelRequest',
  'FinancialParcelResponse' => 'KaribooStructFinancialParcelResponse',
  'HistoryStatus' => 'KaribooStructHistoryStatus',
  'Holidays' => 'KaribooStructHolidays',
  'LabelConfiguration' => 'KaribooStructLabelConfiguration',
  'OpeningHours' => 'KaribooStructOpeningHours',
  'OperationType' => 'KaribooEnumOperationType',
  'PackageType' => 'KaribooEnumPackageType',
  'PickUpPoint' => 'KaribooStructPickUpPoint',
  'Product' => 'KaribooStructProduct',
  'ResponseShipments' => 'KaribooStructResponseShipments',
  'SearchCriteria' => 'KaribooStructSearchCriteria',
  'Shipment' => 'KaribooStructShipment',
  'SpotAroundMeRequest' => 'KaribooStructSpotAroundMeRequest',
  'SpotRequest' => 'KaribooStructSpotRequest',
  'SpotResponse' => 'KaribooStructSpotResponse',
  'StatusDescriptions' => 'KaribooStructStatusDescriptions',
  'Tracking' => 'KaribooStructTracking',
  'TrackingContactInformations' => 'KaribooStructTrackingContactInformations',
  'TrackingRequest' => 'KaribooStructTrackingRequest',
  'TrackingResponse' => 'KaribooStructTrackingResponse',
  'UserLanguage' => 'KaribooEnumUserLanguage',
  'labelChoiceFormat' => 'KaribooEnumLabelChoiceFormat',
  'labelChoiceType' => 'KaribooEnumLabelChoiceType',
  'yesNo' => 'KaribooEnumYesNo',
);
    }
}
