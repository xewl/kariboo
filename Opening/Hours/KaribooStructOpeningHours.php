<?php
/**
 * File for class KaribooStructOpeningHours
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
/**
 * This class stands for KaribooStructOpeningHours originally named OpeningHours
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://api.kariboo.be/Service.svc?xsd=xsd4}
 * @package Kariboo
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-01
 */
class KaribooStructOpeningHours extends KaribooWsdlClass
{
    /**
     * The HD1LU
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1LU;
    /**
     * The HF1LU
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1LU;
    /**
     * The HD2LU
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2LU;
    /**
     * The HF2LU
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2LU;
    /**
     * The HD1MA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1MA;
    /**
     * The HF1MA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1MA;
    /**
     * The HD2MA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2MA;
    /**
     * The HF2MA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2MA;
    /**
     * The HD1ME
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1ME;
    /**
     * The HF1ME
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1ME;
    /**
     * The HD2ME
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2ME;
    /**
     * The HF2ME
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2ME;
    /**
     * The HD1JE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1JE;
    /**
     * The HF1JE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1JE;
    /**
     * The HD2JE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2JE;
    /**
     * The HF2JE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2JE;
    /**
     * The HD1VE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1VE;
    /**
     * The HF1VE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1VE;
    /**
     * The HD2VE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2VE;
    /**
     * The HF2VE
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2VE;
    /**
     * The HD1SA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1SA;
    /**
     * The HF1SA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1SA;
    /**
     * The HD2SA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2SA;
    /**
     * The HF2SA
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2SA;
    /**
     * The HD1DI
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD1DI;
    /**
     * The HF1DI
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF1DI;
    /**
     * The HD2DI
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HD2DI;
    /**
     * The HF2DI
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $HF2DI;
    /**
     * Constructor method for OpeningHours
     * @see parent::__construct()
     * @param string $_hD1LU
     * @param string $_hF1LU
     * @param string $_hD2LU
     * @param string $_hF2LU
     * @param string $_hD1MA
     * @param string $_hF1MA
     * @param string $_hD2MA
     * @param string $_hF2MA
     * @param string $_hD1ME
     * @param string $_hF1ME
     * @param string $_hD2ME
     * @param string $_hF2ME
     * @param string $_hD1JE
     * @param string $_hF1JE
     * @param string $_hD2JE
     * @param string $_hF2JE
     * @param string $_hD1VE
     * @param string $_hF1VE
     * @param string $_hD2VE
     * @param string $_hF2VE
     * @param string $_hD1SA
     * @param string $_hF1SA
     * @param string $_hD2SA
     * @param string $_hF2SA
     * @param string $_hD1DI
     * @param string $_hF1DI
     * @param string $_hD2DI
     * @param string $_hF2DI
     * @return KaribooStructOpeningHours
     */
    public function __construct($_hD1LU = NULL,$_hF1LU = NULL,$_hD2LU = NULL,$_hF2LU = NULL,$_hD1MA = NULL,$_hF1MA = NULL,$_hD2MA = NULL,$_hF2MA = NULL,$_hD1ME = NULL,$_hF1ME = NULL,$_hD2ME = NULL,$_hF2ME = NULL,$_hD1JE = NULL,$_hF1JE = NULL,$_hD2JE = NULL,$_hF2JE = NULL,$_hD1VE = NULL,$_hF1VE = NULL,$_hD2VE = NULL,$_hF2VE = NULL,$_hD1SA = NULL,$_hF1SA = NULL,$_hD2SA = NULL,$_hF2SA = NULL,$_hD1DI = NULL,$_hF1DI = NULL,$_hD2DI = NULL,$_hF2DI = NULL)
    {
        parent::__construct(array('HD1LU'=>$_hD1LU,'HF1LU'=>$_hF1LU,'HD2LU'=>$_hD2LU,'HF2LU'=>$_hF2LU,'HD1MA'=>$_hD1MA,'HF1MA'=>$_hF1MA,'HD2MA'=>$_hD2MA,'HF2MA'=>$_hF2MA,'HD1ME'=>$_hD1ME,'HF1ME'=>$_hF1ME,'HD2ME'=>$_hD2ME,'HF2ME'=>$_hF2ME,'HD1JE'=>$_hD1JE,'HF1JE'=>$_hF1JE,'HD2JE'=>$_hD2JE,'HF2JE'=>$_hF2JE,'HD1VE'=>$_hD1VE,'HF1VE'=>$_hF1VE,'HD2VE'=>$_hD2VE,'HF2VE'=>$_hF2VE,'HD1SA'=>$_hD1SA,'HF1SA'=>$_hF1SA,'HD2SA'=>$_hD2SA,'HF2SA'=>$_hF2SA,'HD1DI'=>$_hD1DI,'HF1DI'=>$_hF1DI,'HD2DI'=>$_hD2DI,'HF2DI'=>$_hF2DI),false);
    }
    /**
     * Get HD1LU value
     * @return string|null
     */
    public function getHD1LU()
    {
        return $this->HD1LU;
    }
    /**
     * Set HD1LU value
     * @param string $_hD1LU the HD1LU
     * @return string
     */
    public function setHD1LU($_hD1LU)
    {
        return ($this->HD1LU = $_hD1LU);
    }
    /**
     * Get HF1LU value
     * @return string|null
     */
    public function getHF1LU()
    {
        return $this->HF1LU;
    }
    /**
     * Set HF1LU value
     * @param string $_hF1LU the HF1LU
     * @return string
     */
    public function setHF1LU($_hF1LU)
    {
        return ($this->HF1LU = $_hF1LU);
    }
    /**
     * Get HD2LU value
     * @return string|null
     */
    public function getHD2LU()
    {
        return $this->HD2LU;
    }
    /**
     * Set HD2LU value
     * @param string $_hD2LU the HD2LU
     * @return string
     */
    public function setHD2LU($_hD2LU)
    {
        return ($this->HD2LU = $_hD2LU);
    }
    /**
     * Get HF2LU value
     * @return string|null
     */
    public function getHF2LU()
    {
        return $this->HF2LU;
    }
    /**
     * Set HF2LU value
     * @param string $_hF2LU the HF2LU
     * @return string
     */
    public function setHF2LU($_hF2LU)
    {
        return ($this->HF2LU = $_hF2LU);
    }
    /**
     * Get HD1MA value
     * @return string|null
     */
    public function getHD1MA()
    {
        return $this->HD1MA;
    }
    /**
     * Set HD1MA value
     * @param string $_hD1MA the HD1MA
     * @return string
     */
    public function setHD1MA($_hD1MA)
    {
        return ($this->HD1MA = $_hD1MA);
    }
    /**
     * Get HF1MA value
     * @return string|null
     */
    public function getHF1MA()
    {
        return $this->HF1MA;
    }
    /**
     * Set HF1MA value
     * @param string $_hF1MA the HF1MA
     * @return string
     */
    public function setHF1MA($_hF1MA)
    {
        return ($this->HF1MA = $_hF1MA);
    }
    /**
     * Get HD2MA value
     * @return string|null
     */
    public function getHD2MA()
    {
        return $this->HD2MA;
    }
    /**
     * Set HD2MA value
     * @param string $_hD2MA the HD2MA
     * @return string
     */
    public function setHD2MA($_hD2MA)
    {
        return ($this->HD2MA = $_hD2MA);
    }
    /**
     * Get HF2MA value
     * @return string|null
     */
    public function getHF2MA()
    {
        return $this->HF2MA;
    }
    /**
     * Set HF2MA value
     * @param string $_hF2MA the HF2MA
     * @return string
     */
    public function setHF2MA($_hF2MA)
    {
        return ($this->HF2MA = $_hF2MA);
    }
    /**
     * Get HD1ME value
     * @return string|null
     */
    public function getHD1ME()
    {
        return $this->HD1ME;
    }
    /**
     * Set HD1ME value
     * @param string $_hD1ME the HD1ME
     * @return string
     */
    public function setHD1ME($_hD1ME)
    {
        return ($this->HD1ME = $_hD1ME);
    }
    /**
     * Get HF1ME value
     * @return string|null
     */
    public function getHF1ME()
    {
        return $this->HF1ME;
    }
    /**
     * Set HF1ME value
     * @param string $_hF1ME the HF1ME
     * @return string
     */
    public function setHF1ME($_hF1ME)
    {
        return ($this->HF1ME = $_hF1ME);
    }
    /**
     * Get HD2ME value
     * @return string|null
     */
    public function getHD2ME()
    {
        return $this->HD2ME;
    }
    /**
     * Set HD2ME value
     * @param string $_hD2ME the HD2ME
     * @return string
     */
    public function setHD2ME($_hD2ME)
    {
        return ($this->HD2ME = $_hD2ME);
    }
    /**
     * Get HF2ME value
     * @return string|null
     */
    public function getHF2ME()
    {
        return $this->HF2ME;
    }
    /**
     * Set HF2ME value
     * @param string $_hF2ME the HF2ME
     * @return string
     */
    public function setHF2ME($_hF2ME)
    {
        return ($this->HF2ME = $_hF2ME);
    }
    /**
     * Get HD1JE value
     * @return string|null
     */
    public function getHD1JE()
    {
        return $this->HD1JE;
    }
    /**
     * Set HD1JE value
     * @param string $_hD1JE the HD1JE
     * @return string
     */
    public function setHD1JE($_hD1JE)
    {
        return ($this->HD1JE = $_hD1JE);
    }
    /**
     * Get HF1JE value
     * @return string|null
     */
    public function getHF1JE()
    {
        return $this->HF1JE;
    }
    /**
     * Set HF1JE value
     * @param string $_hF1JE the HF1JE
     * @return string
     */
    public function setHF1JE($_hF1JE)
    {
        return ($this->HF1JE = $_hF1JE);
    }
    /**
     * Get HD2JE value
     * @return string|null
     */
    public function getHD2JE()
    {
        return $this->HD2JE;
    }
    /**
     * Set HD2JE value
     * @param string $_hD2JE the HD2JE
     * @return string
     */
    public function setHD2JE($_hD2JE)
    {
        return ($this->HD2JE = $_hD2JE);
    }
    /**
     * Get HF2JE value
     * @return string|null
     */
    public function getHF2JE()
    {
        return $this->HF2JE;
    }
    /**
     * Set HF2JE value
     * @param string $_hF2JE the HF2JE
     * @return string
     */
    public function setHF2JE($_hF2JE)
    {
        return ($this->HF2JE = $_hF2JE);
    }
    /**
     * Get HD1VE value
     * @return string|null
     */
    public function getHD1VE()
    {
        return $this->HD1VE;
    }
    /**
     * Set HD1VE value
     * @param string $_hD1VE the HD1VE
     * @return string
     */
    public function setHD1VE($_hD1VE)
    {
        return ($this->HD1VE = $_hD1VE);
    }
    /**
     * Get HF1VE value
     * @return string|null
     */
    public function getHF1VE()
    {
        return $this->HF1VE;
    }
    /**
     * Set HF1VE value
     * @param string $_hF1VE the HF1VE
     * @return string
     */
    public function setHF1VE($_hF1VE)
    {
        return ($this->HF1VE = $_hF1VE);
    }
    /**
     * Get HD2VE value
     * @return string|null
     */
    public function getHD2VE()
    {
        return $this->HD2VE;
    }
    /**
     * Set HD2VE value
     * @param string $_hD2VE the HD2VE
     * @return string
     */
    public function setHD2VE($_hD2VE)
    {
        return ($this->HD2VE = $_hD2VE);
    }
    /**
     * Get HF2VE value
     * @return string|null
     */
    public function getHF2VE()
    {
        return $this->HF2VE;
    }
    /**
     * Set HF2VE value
     * @param string $_hF2VE the HF2VE
     * @return string
     */
    public function setHF2VE($_hF2VE)
    {
        return ($this->HF2VE = $_hF2VE);
    }
    /**
     * Get HD1SA value
     * @return string|null
     */
    public function getHD1SA()
    {
        return $this->HD1SA;
    }
    /**
     * Set HD1SA value
     * @param string $_hD1SA the HD1SA
     * @return string
     */
    public function setHD1SA($_hD1SA)
    {
        return ($this->HD1SA = $_hD1SA);
    }
    /**
     * Get HF1SA value
     * @return string|null
     */
    public function getHF1SA()
    {
        return $this->HF1SA;
    }
    /**
     * Set HF1SA value
     * @param string $_hF1SA the HF1SA
     * @return string
     */
    public function setHF1SA($_hF1SA)
    {
        return ($this->HF1SA = $_hF1SA);
    }
    /**
     * Get HD2SA value
     * @return string|null
     */
    public function getHD2SA()
    {
        return $this->HD2SA;
    }
    /**
     * Set HD2SA value
     * @param string $_hD2SA the HD2SA
     * @return string
     */
    public function setHD2SA($_hD2SA)
    {
        return ($this->HD2SA = $_hD2SA);
    }
    /**
     * Get HF2SA value
     * @return string|null
     */
    public function getHF2SA()
    {
        return $this->HF2SA;
    }
    /**
     * Set HF2SA value
     * @param string $_hF2SA the HF2SA
     * @return string
     */
    public function setHF2SA($_hF2SA)
    {
        return ($this->HF2SA = $_hF2SA);
    }
    /**
     * Get HD1DI value
     * @return string|null
     */
    public function getHD1DI()
    {
        return $this->HD1DI;
    }
    /**
     * Set HD1DI value
     * @param string $_hD1DI the HD1DI
     * @return string
     */
    public function setHD1DI($_hD1DI)
    {
        return ($this->HD1DI = $_hD1DI);
    }
    /**
     * Get HF1DI value
     * @return string|null
     */
    public function getHF1DI()
    {
        return $this->HF1DI;
    }
    /**
     * Set HF1DI value
     * @param string $_hF1DI the HF1DI
     * @return string
     */
    public function setHF1DI($_hF1DI)
    {
        return ($this->HF1DI = $_hF1DI);
    }
    /**
     * Get HD2DI value
     * @return string|null
     */
    public function getHD2DI()
    {
        return $this->HD2DI;
    }
    /**
     * Set HD2DI value
     * @param string $_hD2DI the HD2DI
     * @return string
     */
    public function setHD2DI($_hD2DI)
    {
        return ($this->HD2DI = $_hD2DI);
    }
    /**
     * Get HF2DI value
     * @return string|null
     */
    public function getHF2DI()
    {
        return $this->HF2DI;
    }
    /**
     * Set HF2DI value
     * @param string $_hF2DI the HF2DI
     * @return string
     */
    public function setHF2DI($_hF2DI)
    {
        return ($this->HF2DI = $_hF2DI);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see KaribooWsdlClass::__set_state()
     * @uses KaribooWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return KaribooStructOpeningHours
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
